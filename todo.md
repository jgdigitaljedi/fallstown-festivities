# TODO

## Global style

- get some images
- determine standard style system values for
  - gutters
  - more colors
- create templates

## get data

- pull from calendar scraper
- start building business data
  - restaurants
  - entertainment
  - more later, get those first 2 going
    - bars
- figure out how I will work with data

## build pages

- index
- festivities
  - downloadable and hosted ICS files
  - filterable calendar view
- businesses
  - categories
- about
- 404
- privacy policy
- info
  - where to pay bills
  - lake levels
  - more city specific things

## infra

- PWA
- sitemap
- build optimizations
- some kind of analytics (not Google)
- SEO

## misc

- make nav a lot better, just spit out a starter so far
