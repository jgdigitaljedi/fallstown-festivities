import type { GatsbyConfig } from 'gatsby';
const siteUrl = process.env.URL || `https://fallstownfestivities.com`;

const config: GatsbyConfig = {
  flags: {
    PARTIAL_HYDRATION: true,
  },
  siteMetadata: {
    title: `Fallstown Festivities`,
    siteUrl: `https://fallstownfestivities.com`,
    description: 'Your place to find all things Wichita Falls!',
    image: '/src/images/logos/FallstownFestivities_logo_large_dark.png',
  },
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  trailingSlash: 'never',
  plugins: [
    {
      resolve: '@chakra-ui/gatsby-plugin',
      options: {
        resetCSS: true,
        portalZIndex: 40,
      },
    },
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        google: {
          families: ['Oswald', 'Lora'],
        },
      },
    },
    {
      resolve: 'gatsby-plugin-sitemap',
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Fallstown Festivities`,
        short_name: `WF Festivities`,
        start_url: `/`,
        background_color: `#ebf4fa`,
        theme_color: `#C44708`,
        display: `standalone`,
        icon: `src/images/favicons/favicon.png`,
        icons: [
          {
            src: 'src/images/favicons/android-chrome-192x192.png',
            sizes: '192x192',
            type: 'image/png',
          },
          {
            src: 'src/images/faviconsc/android-chrome-512x512.png',
            sizes: '512x512',
            type: 'image/png',
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-sanity`,
      options: {
        projectId: `v0wof3fu`,
        dataset: `events`,
        token: process.env.SANITY_ACCESS_TOKEN,
      },
    },
    {
      resolve: `gatsby-source-sanity`,
      options: {
        projectId: `v0wof3fu`,
        dataset: `businesses`,
        token: process.env.SANITY_ACCESS_TOKEN,
      },
    },
    `gatsby-plugin-image`,
    {
      resolve: 'gatsby-plugin-html-attributes',
      options: {
        lang: 'en',
      },
    },
    `gatsby-plugin-smoothscroll`,
  ],
};

export default config;
