import { Tab, TabList } from '@chakra-ui/react';
import React from 'react';
import { typeToDisplay } from '../../util/businessHelpers';

interface BusinessTabsProps {
  tabs: string[];
}

const BusinessTabs: React.FC<BusinessTabsProps> = ({ tabs }) => {
  return (
    <TabList>
      {tabs.map((tab) => (
        <Tab
          key={tab}
          value={tab}
          sx={{
            textTransform: 'capitalize',
            fontFamily: 'heading',
            fontSize: '1.5rem',
          }}
          _selected={{ backgroundColor: 'bg2' }}
        >
          {typeToDisplay[tab as keyof typeof typeToDisplay]}
        </Tab>
      ))}
    </TabList>
  );
};

export default BusinessTabs;
