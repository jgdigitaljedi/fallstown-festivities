import React from 'react';
import { SanityBusiness } from '../../util/types';
import {
  Box,
  Card,
  CardBody,
  CardHeader,
  Divider,
  Flex,
  Heading,
  HStack,
  Icon,
  Link,
  Stack,
  Text,
  useColorMode,
} from '@chakra-ui/react';
import { FaMap } from '@react-icons/all-files/fa/FaMap';
import { FaPhone } from '@react-icons/all-files/fa/FaPhone';
import Ratings from './Ratings';
import { FaDollarSign } from '@react-icons/all-files/fa/FaDollarSign';
import HoursDisplay from './HoursDisplay';
import BusinessOpen from './BusinessOpen';
import { FaListAlt } from '@react-icons/all-files/fa/FaListAlt';
import { FaYelp } from '@react-icons/all-files/fa/FaYelp';

interface BusinessCardProps {
  business: SanityBusiness;
}

const BusinessCard: React.FC<BusinessCardProps> = ({ business }) => {
  const { colorMode } = useColorMode();
  const getAddressLine = (bus: any) => {
    if (bus.coordinates?.lat && bus.coordinates?.lon) {
      return (
        <Box>
          <Flex alignItems={'center'}>
            <Link
              href={`geo:${bus.coordinates.lat},${bus.coordinates.lon}`}
              sx={{
                display: 'flex',
                alignItems: 'center',
                fontStyle: 'italic',
                mr: 2,
                mb: '.2rem',
              }}
              target="_blank"
            >
              <FaMap style={{ marginRight: '1rem' }} />
              {bus.location}
            </Link>
          </Flex>
        </Box>
      );
    } else {
      return (
        <Box>
          <Flex alignItems={'center'} marginBottom=".2rem">
            <Text>{bus.location}</Text>
          </Flex>
        </Box>
      );
    }
  };
  return (
    <Card
      sx={{
        height: '100%',
        boxShadow: 'elevated',
        bgGradient:
          colorMode === 'dark'
            ? 'radial-gradient(circle at center, bg2 0, bg4 100%)'
            : 'radial-gradient(circle at center, bg2 0, bg1 100%)',
      }}
    >
      <CardHeader sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <Heading as="h3" size="md" color="textInfo">
          {business.name}
        </Heading>
        {business.price && (
          <Flex color="cashola">
            {Array.apply(null, new Array(business.price.length)).map((x, i) => (
              <FaDollarSign
                key={`${business.id}-price${i}`}
                style={{ width: `1rem`, height: `1rem` }}
              />
            ))}
          </Flex>
        )}
      </CardHeader>
      <Divider />
      <CardBody>
        <Stack>
          <BusinessOpen hours={business.hours} />
          <Ratings count={business.reviewCount} rating={business.rating} />
          <Box>{getAddressLine(business)}</Box>
          <Link
            href={`tel://${business.phone.replace(/\D/g, '')}`}
            sx={{ display: 'flex', alignItems: 'center', mb: '.2rem' }}
          >
            <FaPhone style={{ marginRight: '1rem' }} />
            {business.displayPhone}
          </Link>
          {!!business.menuUrl && (
            <Link
              href={business.menuUrl}
              isExternal
              target="_blank"
              sx={{ display: 'flex', alignItems: 'center', mb: '.2rem' }}
            >
              <Icon as={FaListAlt} sx={{ marginRight: '1rem' }} /> Menu
            </Link>
          )}
          <Link
            href={business.yelpUrl}
            isExternal
            target="_blank"
            sx={{ display: 'flex', alignItems: 'center', mb: '.2rem' }}
          >
            <Icon as={FaYelp} sx={{ marginRight: '1rem' }} /> View on Yelp!
          </Link>
          <Text sx={{ fontStyle: 'italic', color: 'textInfo' }}>
            {[...business.categories, ...business.transactions].join(', ')}
          </Text>

          <HoursDisplay
            hours={business.hours}
            id={business.id}
            colorMode={colorMode}
          />
        </Stack>
      </CardBody>
    </Card>
  );
};

export default BusinessCard;
