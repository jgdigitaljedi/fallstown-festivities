import React from 'react';
import { BusinessHours } from '../../util/types';
import { isBusinessOpen } from '../../util/businessHelpers';
import { Flex, Icon, keyframes, Text, useTheme } from '@chakra-ui/react';
import { FaDoorOpen } from '@react-icons/all-files/fa/FaDoorOpen';
import { FaDoorClosed } from '@react-icons/all-files/fa/FaDoorClosed';

interface BusinessOpenProps {
  hours: BusinessHours[];
}

const wrapperStyles = {
  alignItems: 'center',
};
const iconStyles = { mr: 2 };
const openClosedStyles = {
  fontWeight: 'bold',
};

const BusinessOpen: React.FC<BusinessOpenProps> = ({ hours }) => {
  const isOpen = isBusinessOpen(hours);
  const theme = useTheme();
  const greenGlowAnimation = keyframes({
    from: {
      textShadow: `0 0 3px ${theme.colors.gray[900]}, 0 0 6px ${theme.colors.gray[900]}, 0 0 9px ${theme.colors.green[700]}, 0 0 12px ${theme.colors.green[800]}, 0 0 15px ${theme.colors.green[800]}, 0 0 18px ${theme.colors.green[800]}, 0 0 21px ${theme.colors.green[800]}, 0 0 24px ${theme.colors.green[800]}`,
    },
    to: {
      textShadow: `0 0 6px ${theme.colors.gray[900]}, 0 0 9px ${theme.colors.green[300]}, 0 0 12px ${theme.colors.green[300]}, 0 0 15px ${theme.colors.green[300]}, 0 0 18px ${theme.colors.green[300]}, 0 0 21px ${theme.colors.green[300]}, 0 0 24px ${theme.colors.green[300]}`,
    },
  });

  if (isOpen) {
    return (
      <Flex sx={wrapperStyles}>
        <Icon
          as={FaDoorOpen}
          sx={{
            ...iconStyles,
            color: 'green.500',
            animation: `${greenGlowAnimation} 2s ease-in-out infinite alternate`,
          }}
        />
        <Text
          sx={{
            ...openClosedStyles,
            color: 'green.500',
            animation: `${greenGlowAnimation} 2s ease-in-out infinite alternate`,
          }}
        >
          Open
        </Text>
      </Flex>
    );
  }
  return (
    <Flex sx={wrapperStyles}>
      <Icon as={FaDoorClosed} sx={{ ...iconStyles, color: 'red.500' }} />
      <Text sx={{ ...openClosedStyles, color: 'red.500', fontStyle: 'italic' }}>
        Closed
      </Text>
    </Flex>
  );
};

export default BusinessOpen;
