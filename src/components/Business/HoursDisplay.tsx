import React, { useMemo } from 'react';
import { BusinessHours } from '../../util/types';
import { Box, Stack, Text } from '@chakra-ui/react';
import { daysInOrder } from '../../util/constants';

interface HoursDisplayProps {
  hours: BusinessHours[];
  id: string;
  colorMode: 'light' | 'dark';
}

const HoursDisplay: React.FC<HoursDisplayProps> = ({
  hours,
  id,
  colorMode,
}) => {
  const hasHours = useMemo(() => {
    return hours && hours.filter((h) => h.display !== 'Closed').length > 0;
  }, [hours]);
  const hoursOrdered = () => {
    const orderedHours = daysInOrder.map((day) => {
      const hour = hours.find((h) => h.day === day);
      return hour;
    });
    return orderedHours as BusinessHours[];
  };

  if (hasHours) {
    return (
      <Stack
        sx={{
          borderColor: 'orange.500',
          borderStyle: 'solid',
          borderWidth: '1px',
          borderRadius: 'md',
          p: 4,
          backgroundColor:
            colorMode === 'dark'
              ? 'rgba(0, 0, 0, 0.2)'
              : 'rgba(255, 255, 255, 0.2)',
        }}
      >
        <Text
          fontSize="md"
          fontWeight="bold"
          color="textHighlight2"
          textAlign="center"
        >
          Hours of operation
        </Text>
        {hoursOrdered().map((hour) => (
          <Box key={`${id}-${hour.day}`}>
            {hour.display === 'Closed' ? `${hour.day}: Closed` : hour.display}
          </Box>
        ))}
      </Stack>
    );
  }

  return (
    <Box>
      <Text sx={{ color: 'textHighlight2', fontStyle: 'italic' }}>
        No hours info available
      </Text>
    </Box>
  );
};

export default HoursDisplay;
