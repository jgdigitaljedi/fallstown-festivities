import { Box, Flex, Text } from '@chakra-ui/react';
import React from 'react';
import { FaStar } from '@react-icons/all-files/fa/FaStar';
import { FaRegStar } from '@react-icons/all-files/fa/FaRegStar';

interface RatingsProps {
  count: number;
  rating: number;
}

const Ratings: React.FC<RatingsProps> = ({ count, rating }) => {
  const getStars = () => {
    const fullStars = Math.floor(rating);
    const partialPercentage = !!(rating % 1)
      ? parseInt((rating * 10).toString().slice(-1)) * 10
      : 0;
    if (!!count) {
      return (
        <>
          {Array.apply(null, Array(fullStars)).map((x, i) => (
            <FaStar key={i} style={{ width: `1rem`, height: `1rem` }} />
          ))}
          {partialPercentage > 0 && (
            <Box width="1rem">
              <Box sx={{ width: `${partialPercentage}%`, overflow: 'hidden' }}>
                <FaStar style={{ width: `1rem`, height: `1rem` }} />
              </Box>
            </Box>
          )}
        </>
      );
    } else {
      return <Text>No reviews yet</Text>;
    }
  };

  return (
    <Flex sx={{ alignItems: 'baseline' }}>
      <Flex
        sx={{
          flexDirection: 'row',
          width: count ? '5rem' : '100%',
          color: 'ratings',
        }}
      >
        {getStars()}
      </Flex>
      <Text
        sx={{
          ml: 2,
          fontWeight: 'bold',
          fontStyle: 'italic',
          color: 'textHighlight',
        }}
      >
        {!!count && `${rating} out of ${count} reviews`}
      </Text>
    </Flex>
  );
};

export default Ratings;
