import React, { useEffect, useState } from 'react';
import { SanityBusiness } from '../../../util/types';
import {
  Box,
  Card,
  CardBody,
  CardHeader,
  Flex,
  Heading,
  useConst,
} from '@chakra-ui/react';
import SearchBar from './SearchBar';
import { filterByCategory, getCategories } from '../../../util/businessHelpers';
import CurrentlyOpen from './CurrentlyOpen';
import Combobox from '../../core/Combobox';
import { Option } from 'chakra-multiselect';

interface FiltersWrapperProps {
  businesses: SanityBusiness[];
  activeTab: string;
  onFiltersChange: (data: any) => void;
}

const FiltersWrapper: React.FC<FiltersWrapperProps> = ({
  businesses,
  activeTab,
  onFiltersChange,
}) => {
  const [catSelections, setCatSelections] = useState<Option[]>([]);
  const onSearchChange = (searchStr: string) => {};
  const categoriesList = useConst(
    getCategories(businesses || [])
      .sort()
      .map((o) => ({
        label: o,
        value: o,
      }))
  );

  const onCategoriesChange = (cats: Option | Option[]) => {
    if (Array.isArray(cats)) {
      setCatSelections(cats);
      onFiltersChange(filterByCategory(businesses, cats));
    } else {
      setCatSelections([cats]);
      onFiltersChange(filterByCategory(businesses, [cats]));
    }
  };

  useEffect(() => {
    setCatSelections(categoriesList);
  }, []);

  return (
    <Card
      sx={{ width: '100%', mb: 10, boxShadow: 'card', backgroundColor: 'bg1' }}
    >
      <CardHeader>
        <Heading as="h3" size="md">
          Filters
        </Heading>
      </CardHeader>
      <CardBody>
        <SearchBar onChange={onSearchChange} />
        <Flex sx={{ flexDirection: ['column', 'column', 'row'] }}>
          <Box sx={{ my: 4 }}>
            <Combobox
              options={categoriesList}
              selections={catSelections}
              onChange={onCategoriesChange}
            />
          </Box>
          <CurrentlyOpen onOpenCloseChange={() => {}} />
        </Flex>
      </CardBody>
    </Card>
  );
};

export default FiltersWrapper;
