import React from 'react';
import { SanityBusiness } from '../../../util/types';
import { FormControl, FormLabel, Switch } from '@chakra-ui/react';

interface CurrentlyOpenProps {
  onOpenCloseChange: (isOpen: boolean) => void;
}

const CurrentlyOpen: React.FC<CurrentlyOpenProps> = ({ onOpenCloseChange }) => {
  return (
    <FormControl>
      <FormLabel htmlFor="currently-open">Currently Open</FormLabel>
      <Switch
        id="currently-open"
        onChange={(e) => onOpenCloseChange(e.target.checked)}
      />
    </FormControl>
  );
};

export default CurrentlyOpen;
