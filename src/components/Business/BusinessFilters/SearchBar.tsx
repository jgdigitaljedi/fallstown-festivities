import { Input, InputGroup, InputLeftElement } from '@chakra-ui/react';
import { FaSearch } from '@react-icons/all-files/fa/FaSearch';
import React from 'react';

interface SearchBarProps {
  onChange: (searchStr: string) => void;
}

const SearchBar: React.FC<SearchBarProps> = ({ onChange }) => {
  const onChangDebounce = (event: React.ChangeEvent<HTMLInputElement>) => {};
  return (
    <InputGroup sx={{ width: '100%' }}>
      <InputLeftElement pointerEvents="none">
        <FaSearch />
      </InputLeftElement>
      <Input type="text" onChange={onChangDebounce} />
    </InputGroup>
  );
};

export default SearchBar;
