import {
  Grid,
  GridItem,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
} from '@chakra-ui/react';
import React from 'react';
import { SanityBusiness } from '../../util/types';
import BusinessCard from './BusinessCard';
import { maxWidth } from '../../util/styles';

interface BusinessPanelProps {
  tab: string;
  data: SanityBusiness[];
}

const BusinessPanel: React.FC<BusinessPanelProps> = ({ tab, data }) => {
  return (
    <TabPanel sx={{ width: '100%', maxWidth, px: 0 }}>
      <Grid
        templateColumns={{
          base: 'repeat(1, minmax(0, 1fr))',
          md: 'repeat(1, minmax(0, 1fr))',
          lg: 'repeat(2, minmax(0, 1fr))',
          '2xl': 'repeat(3, minmax(0, 1fr))',
        }}
        gap={6}
        sx={{
          justifyItems: 'center',
          alignItems: 'center',
          width: '100%',
          mb: 4,
        }}
      >
        {(data || []).map((b: SanityBusiness) => (
          <GridItem key={`${b.id}-${tab}`} height={'100%'} width={'100%'}>
            <BusinessCard business={b} />
          </GridItem>
        ))}
      </Grid>
    </TabPanel>
  );
};

export default BusinessPanel;
