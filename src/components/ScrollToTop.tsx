import React, { useEffect } from 'react';
import scrollTo from 'gatsby-plugin-smoothscroll';
import { Button } from '@chakra-ui/react';
import { FaArrowUp } from '@react-icons/all-files/fa/FaArrowUp';

const ScrollToTop: React.FC = () => {
  const [showButton, setShowButton] = React.useState(false);
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 300) {
        setShowButton(true);
      } else {
        setShowButton(false);
      }
    };
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  });

  return (
    <>
      {showButton && (
        <Button
          variant="circle"
          onClick={() => scrollTo('#top')}
          sx={{
            position: 'fixed',
            bottom: '2rem',
            right: '2rem',
            zIndex: 100,
          }}
          boxShadow="md"
          type="button"
          aria-label="Scroll to top"
        >
          <FaArrowUp />
        </Button>
      )}
    </>
  );
};

export default ScrollToTop;
