import { Image, Skeleton, SystemStyleObject } from '@chakra-ui/react';
import React, { useMemo, useState } from 'react';

interface SmartImageProps {
  src: string;
  alt: string;
  loaderSize: {
    w: number;
    h: number;
  };
  loading?: 'lazy' | 'eager' | undefined;
  sxProps?: SystemStyleObject;
}

const SmartImage: React.FC<SmartImageProps> = ({
  src,
  alt,
  loaderSize,
  loading,
  sxProps,
}) => {
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const imageStyle: SystemStyleObject = useMemo(() => {
    return {
      ...sxProps,
      display: isLoaded ? 'block' : 'none',
      maxWidth: loaderSize.w,
    };
  }, [isLoaded]);
  return (
    <>
      {!isLoaded && (
        <Skeleton height={`${loaderSize.w}px`} width={`${loaderSize.h}px`} />
      )}
      <Image
        src={src}
        loading={loading}
        alt={alt}
        sx={imageStyle}
        onLoad={() => setIsLoaded(true)}
      />
    </>
  );
};

export default SmartImage;
