import {
  Input,
  InputGroup,
  InputRightElement,
  BoxProps,
} from '@chakra-ui/react';
import {
  MultiSelect,
  Option,
  SelectionVisibilityMode,
} from 'chakra-multiselect';
import React from 'react';

interface ComboboxProps {
  options: Option[];
  selections: Option[];
  onChange: (selections: Option | Option[]) => void;
}

const Combobox: React.FC<ComboboxProps> = ({
  options,
  selections,
  onChange,
}) => {
  const selectionsLen = () => {
    if (selections.length === options.length) {
      return 'all';
    }
    return selections.length;
  };
  return (
    <MultiSelect
      size="lg"
      options={options}
      value={selections}
      onChange={onChange}
      label={'Filter by categories'}
      labelProps={{ sx: { fontFamily: 'heading', fontWeight: 'bold' } }}
      selectedListProps={{
        multi: true,
        size: 'sm',
        sx: {
          div: {
            display: 'none',
          },
        },
      }}
      searchPlaceholder={`Search (${selectionsLen()} selected)`}
      selectionVisibleIn={SelectionVisibilityMode.List}
      placeholder="Search (0 selected)"
      actionGroupProps={{
        size: 'md',
        sx: {
          position: 'absolute',
          right: 0,
        },
        clearButtonProps: {
          'aria-label': 'Clear',
          size: 'lg',
          variant: 'ghost',
          isDisabled: selectionsLen() === 0,
          sx: {
            p: 0,
            svg: {
              height: '1.5rem',
              width: '1.5rem',
              color: 'red.500',
            },
          },
        },
      }}
    />
  );
};

export default Combobox;
