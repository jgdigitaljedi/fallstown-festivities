import {
  Box,
  IconButton,
  useColorMode,
  Switch,
  Flex,
  Link,
  Icon,
  Drawer,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  DrawerHeader,
  DrawerBody,
  Image,
  LinkBox,
  LinkOverlay,
} from '@chakra-ui/react';
import React, { useCallback, useRef, useState } from 'react';
import { IconType } from '@react-icons/all-files';
import { FaGlobe } from '@react-icons/all-files/fa/FaGlobe';
import { FaHome } from '@react-icons/all-files/fa/FaHome';
import { FaCalendarAlt } from '@react-icons/all-files/fa/FaCalendarAlt';
import { FaBuilding } from '@react-icons/all-files/fa/FaBuilding';
import { FaSun } from '@react-icons/all-files/fa/FaSun';
import { FaMoon } from '@react-icons/all-files/fa/FaMoon';
import { FaInfoCircle } from '@react-icons/all-files/fa/FaInfoCircle';
import { FaBars } from '@react-icons/all-files/fa/FaBars';
// @ts-ignore
import LogoLight from '../images/logos/FallstownFestivities_logo_128_light.png';
// @ts-ignore
import LogoDark from '../images/logos/FallstownFestivities_logo_128_dark.png';

interface NavObj {
  label: string;
  path: string;
  icon?: IconType;
}

interface NavProps {
  location: Location;
}

const Navbar: React.FC<NavProps> = ({ location }) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const { colorMode, toggleColorMode } = useColorMode();
  const btnRef = useRef(null);

  console.log(location);

  const onMenuOpen = () => {
    setMenuOpen(!menuOpen);
  };

  const links: NavObj[] = [
    {
      label: 'Home',
      path: '/',
      icon: FaHome,
    },
    {
      label: 'Festivities',
      path: '/festivities',
      icon: FaCalendarAlt,
    },
    {
      label: 'Businesses',
      path: '/businesses',
      icon: FaBuilding,
    },
    {
      label: 'Info',
      path: '/info',
      icon: FaInfoCircle,
    },
    {
      label: 'About',
      path: '/about',
      icon: FaGlobe,
    },
  ];

  const Logo = () => {
    return (
      <LinkBox>
        <LinkOverlay href="/">
          <Image
            src={colorMode === 'dark' ? LogoLight : LogoDark}
            alt="Fallstown Festivities logo"
          />
        </LinkOverlay>
      </LinkBox>
    );
  };

  const navStyle = useCallback(
    (link: NavObj, isDrawer?: boolean) => {
      const isCurrent = location.pathname === link.path;
      const linkColor = isDrawer ? 'orange.600' : 'textHighlight';
      return {
        fontFamily: 'heading',
        color: isCurrent ? linkColor : undefined,
        borderBottomWidth: isCurrent ? '1px' : undefined,
        borderBottomStyle: isCurrent ? 'solid' : undefined,
        borderBottomColor: isCurrent ? linkColor : undefined,
        fontSize: isDrawer ? '1.5rem' : '1.25rem',
        ':hover': {
          textDecoration: 'none',
          color: linkColor,
        },
      };
    },
    [location]
  );

  return (
    <Box
      as="nav"
      sx={{
        display: 'flex',
        width: '100%',
        px: 4,
        py: 2,
        justifyContent: 'space-between',
        alignItems: 'center',
        bgColor: 'bg1',
        boxShadow: '0px 2px 5px rgba(0, 0, 0, 0.5)',
      }}
    >
      <IconButton
        aria-label="menu"
        icon={<FaBars />}
        onClick={onMenuOpen}
        sx={{ display: ['flex', 'flex', 'none'], justifyContent: 'center' }}
        ref={btnRef}
        variant="ghost"
        fontSize={24}
      />
      <Drawer
        isOpen={menuOpen}
        placement="left"
        onClose={onMenuOpen}
        finalFocusRef={btnRef}
      >
        <DrawerOverlay />
        <DrawerContent sx={{ bg: 'bg1' }}>
          <DrawerCloseButton />
          <DrawerHeader
            sx={{
              display: 'flex',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            {Logo()}Fallstown Festivities
          </DrawerHeader>
          <DrawerBody
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            {links.map((link) => {
              return (
                <Link
                  href={link.path}
                  m={4}
                  key={link.label}
                  sx={navStyle(link, true)}
                >
                  {link.icon && <Icon mr={2} as={link.icon} />}
                  {link.label}
                </Link>
              );
            })}
          </DrawerBody>
        </DrawerContent>
      </Drawer>
      <Box sx={{ display: ['block', 'block', 'none'] }}>{Logo()}</Box>
      <Flex
        sx={{
          flex: 1,
          justifyContent: 'center',
          display: ['none', 'none', 'flex'],
        }}
      >
        <Box mr={6} pt={1}>
          {Logo()}
        </Box>
        {links.map((link) => {
          return (
            <Link
              href={link.path}
              m={4}
              key={`${link.label}-mobile`}
              sx={navStyle(link)}
            >
              {link.icon && (
                <Icon
                  mr={2}
                  as={link.icon}
                  sx={{ display: ['none', 'none', 'none', 'inline'] }}
                />
              )}
              {link.label}
            </Link>
          );
        })}
      </Flex>
      <Flex sx={{ alignItems: 'center' }}>
        <FaSun />
        <Switch
          isChecked={colorMode === 'dark'}
          onChange={toggleColorMode}
          sx={{ px: 2 }}
          colorScheme="orange"
          aria-label="Toggle dark mode"
        />
        <FaMoon />
      </Flex>
    </Box>
  );
};

export default Navbar;
