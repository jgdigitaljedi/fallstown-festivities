import React from 'react';
import Calendar from '../Calendar';
import { Flex } from '@chakra-ui/react';
import { TabViewProps } from './tabView';

const CalendarTab: React.FC<TabViewProps> = ({ data }) => {
  return (
    <Flex>
      <Calendar data={data} />
    </Flex>
  );
};

export default CalendarTab;
