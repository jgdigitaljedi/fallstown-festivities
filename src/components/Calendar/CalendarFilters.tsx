'use client';

import React, { FormEvent, useState } from 'react';
import { Flex, Input, InputGroup, InputLeftElement } from '@chakra-ui/react';
import { FaSearch } from '@react-icons/all-files/fa/FaSearch';
const CalendarFilters: React.FC = () => {
  const [searchVal, setSearchVal] = useState('');
  const onSearchChange = (event: FormEvent<HTMLInputElement>) => {
    const text = event.currentTarget.value;
    setSearchVal(text);
  };

  return (
    <Flex sx={{ mb: 4 }}>
      <InputGroup>
        <InputLeftElement pointerEvents="none">
          <FaSearch />
        </InputLeftElement>
        <Input
          placeholder="Search"
          sx={{ minWidth: '12rem' }}
          onChange={onSearchChange}
          value={searchVal}
        />
      </InputGroup>
    </Flex>
  );
};

export default CalendarFilters;
