const gridGap = '2px';
const gridHeight = '700px';
export const textColorLight = 'rgba(0, 0, 0, 0.4)';
// const textColorPrimary = 'rgba(0, 0, 0, 0.7)';
// const foregroundColor = 'white';
// const gridBackgroundColor = 'rgb(211, 205, 198)';
const spaceMd = '8px';

export const calendarRoot = {
  width: '100%',
};

/* | Sun | Mon | Tue | Wed | Thu | Fri | Sat  */
export const dayOfWeekHeaderCell = {
  color: 'regularText',
  backgroundColor: 'bg2',
  padding: `${spaceMd} 0`,
  minWidth: 0,
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
  border: 'none',
  gridGap: 0,
};

export const daysOfWeekAndGrid = {
  width: '100%',
  boxSizing: 'border-box',
  display: 'grid',
  gridTemplateColumns: 'repeat(7, 1fr)',
  gridColumnGap: gridGap,
  gridRowGap: gridGap,
};

export const daysGrid = {
  height: gridHeight,
  position: 'relative',
  backgroundColor: 'bg1',
};

export const dayGridItemContainer = {
  position: 'relative',
  backgroundColor: 'bg2',
  display: 'flex',
  flexDirection: 'column',
  color: 'regularText',
  padding: spaceMd,
  paddingBottom: 0,
  flexShrink: 0,
  minHeight: '10rem',
};

/* Position the day label within the day cell */
export const dayGridItemContainerAndHeader = {
  color: 'regularText',
  padding: spaceMd,
  paddingBottom: 0,
  flexShrink: 0,
  fontWeight: 'bold',
};

export const dayGridItemContainerAndContentWrapper = {
  flex: 1,
  minHeight: '0',
  position: 'relative',
};
