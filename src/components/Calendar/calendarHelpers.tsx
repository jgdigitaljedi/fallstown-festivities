import dayjs from 'dayjs';
import isSameOrBeforeMod from 'dayjs/plugin/isSameOrBefore';
import isSameOrAfterMod from 'dayjs/plugin/isSameOrAfter';
import customParseFormatMod from 'dayjs/plugin/customParseFormat';
import {
  CalendarEvent,
  CalendarGridDay,
  CalendarGridDayWithEvents,
} from '../../util/types';

dayjs.extend(isSameOrBeforeMod);
dayjs.extend(isSameOrAfterMod);
dayjs.extend(customParseFormatMod);

export const monthsArr = [
  { label: 'January', value: 1 },
  { label: 'February', value: 2 },
  { label: 'March', value: 3 },
  { label: 'April', value: 4 },
  { label: 'May', value: 5 },
  { label: 'June', value: 6 },
  { label: 'July', value: 7 },
  { label: 'August', value: 8 },
  { label: 'September', value: 9 },
  { label: 'October', value: 10 },
  { label: 'November', value: 11 },
  { label: 'December', value: 12 },
];

export const yearList = (): number[] => {
  const currentYear = new Date().getFullYear();
  const nextYear = currentYear + 1;
  return [currentYear, nextYear];
};

export const daysOfWeek: string[] = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

export function getNumberOfDaysInMonth(year: number, month: number): number {
  return dayjs(`${year}-${month}-01`).daysInMonth();
}

export function createDaysForCurrentMonth(
  year: number,
  month: number
): CalendarGridDay[] {
  return [...Array(getNumberOfDaysInMonth(year, month))].map((_, index) => {
    return {
      dateString: dayjs(`${year}-${month}-${index + 1}`).format('MM-DD-YYYY'),
      dayOfMonth: index + 1,
      isCurrentMonth: true,
    };
  });
}

export function createDaysForPreviousMonth(
  year: number,
  month: number,
  currentMonthDays: any[]
): CalendarGridDay[] {
  const firstDayOfTheMonthWeekday = getWeekday(currentMonthDays[0].dateString);
  const previousMonth = dayjs(`${year}-${month}-01`).subtract(1, 'month');

  const visibleNumberOfDaysFromPreviousMonth = firstDayOfTheMonthWeekday;

  const previousMonthLastMondayDayOfMonth = dayjs(
    currentMonthDays[0].dateString
  )
    .subtract(visibleNumberOfDaysFromPreviousMonth, 'day')
    .date();

  return [...Array(visibleNumberOfDaysFromPreviousMonth)].map((_, index) => {
    return {
      dateString: dayjs(
        `${previousMonth.year()}-${previousMonth.month() + 1}-${
          previousMonthLastMondayDayOfMonth + index
        }`
      ).format('MM-DD-YYYY'),
      dayOfMonth: previousMonthLastMondayDayOfMonth + index,
      isCurrentMonth: false,
      isPreviousMonth: true,
    };
  });
}

export function createDaysForNextMonth(
  year: number,
  month: number,
  currentMonthDays: any[]
): CalendarGridDay[] {
  const lastDayOfTheMonthWeekday = getWeekday(
    `${year}-${month}-${currentMonthDays.length}`
  );
  const nextMonth = dayjs(`${year}-${month}-01`).add(1, 'month');
  const visibleNumberOfDaysFromNextMonth = 6 - lastDayOfTheMonthWeekday;

  return [...Array(visibleNumberOfDaysFromNextMonth)].map((day, index) => {
    return {
      dateString: dayjs(
        `${nextMonth.year()}-${nextMonth.month() + 1}-${index + 1}`
      ).format('MM-DD-YYYY'),
      dayOfMonth: index + 1,
      isCurrentMonth: false,
      isNextMonth: true,
    };
  });
}

// sunday === 0, saturday === 6
export function getWeekday(dateString: string): number {
  return parseInt(dayjs(dateString).format('d'));
}

export function isWeekendDay(dateString: string): boolean {
  return [6, 0].includes(getWeekday(dateString));
}

export const matchEventsToDays = (
  data: CalendarEvent[],
  calendarGridDayObjects: CalendarGridDay[]
): CalendarGridDayWithEvents[] => {
  const calendarWithEvents = calendarGridDayObjects.map(
    (day: CalendarGridDay) => {
      const dayEvents = data.filter((event: CalendarEvent) => {
        return (
          dayjs(event.startTime).isSameOrBefore(day.dateString, 'day') &&
          dayjs(event.endTime).isSameOrAfter(day.dateString, 'day')
        );
      });
      return {
        ...day,
        events: dayEvents,
      };
    }
  );
  return calendarWithEvents;
};
