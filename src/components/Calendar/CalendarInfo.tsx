import {
  Box,
  Button,
  Card,
  CardBody,
  Flex,
  Link,
  LinkBox,
  LinkOverlay,
  Stack,
  Text,
} from '@chakra-ui/react';
import React, { useMemo } from 'react';
import { CalendarEvent, SanityIcs } from '../../util/types';
import { uniqBy } from 'lodash';
import { FaExternalLinkSquareAlt } from '@react-icons/all-files/fa/FaExternalLinkSquareAlt';
import { FaDownload } from '@react-icons/all-files/fa/FaDownload';

interface CalendarInfoProps {
  calendarEvents: CalendarEvent[];
  calendarIcs: SanityIcs;
}

const CalendarInfo: React.FC<CalendarInfoProps> = ({
  calendarEvents,
  calendarIcs,
}) => {
  const sectionStyle = {
    borderWidth: '1px',
    borderStyle: 'solid',
    borderColor: 'gray.300',
    borderRadius: '4px',
    p: 2,
  };

  const calendarsUsed = useMemo(() => {
    const simplified = calendarEvents.map((event) => {
      return {
        calendar: event.calendar,
        website: event.website,
      };
    });
    return uniqBy(simplified, 'calendar');
  }, [calendarEvents]);

  console.log(calendarIcs);

  return (
    <Box>
      <Card marginY={4} backgroundColor="bg1">
        <CardBody>
          <Stack>
            <Box sx={sectionStyle}>
              <Text sx={{ fontSize: '1.2rem', fontWeight: 'bold' }}>
                Calendars included
              </Text>
              <Flex
                sx={{
                  flexWrap: 'wrap',
                  justifyContent: 'space-evenly',
                  flexDirection: ['column', 'column', 'row'],
                }}
              >
                {calendarsUsed.map((calendar) => {
                  return (
                    <Flex key={calendar.calendar} sx={{ my: 2, mx: [1, 1, 3] }}>
                      <Link
                        sx={{
                          textTransform: 'capitalize',
                          display: 'flex',
                          alignItems: 'center',
                        }}
                        href={calendar.website}
                        isExternal
                        target="_blank"
                      >
                        <Text sx={{ mr: 1 }}>{calendar.calendar}</Text>
                        <FaExternalLinkSquareAlt />
                      </Link>
                    </Flex>
                  );
                })}
              </Flex>
            </Box>
            <Flex
              sx={{
                justifyContent: 'space-between',
                alignItems: 'center',
                mt: 2,
              }}
            >
              <Text
                sx={{
                  fontSize: ['1rem', '1rem', '1.2rem'],
                  fontWeight: 'bold',
                }}
              >
                Download ICS file with all events
              </Text>
              <LinkBox>
                <LinkOverlay href={calendarIcs.url}>
                  <Button leftIcon={<FaDownload />}>Download</Button>
                </LinkOverlay>
              </LinkBox>
            </Flex>
          </Stack>
        </CardBody>
      </Card>
    </Box>
  );
};

export default CalendarInfo;
