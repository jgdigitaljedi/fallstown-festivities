import { Box } from '@chakra-ui/react';
import React from 'react';
import { daysOfWeek } from './calendarHelpers';
import { dayOfWeekHeaderCell, daysOfWeekAndGrid } from './CalendarStyles';

const CalendarDays: React.FC = () => {
  return (
    <Box sx={{ ...daysOfWeekAndGrid, gridColumnGap: 0 }}>
      {daysOfWeek.map((day, index) => (
        <Box
          key={day}
          sx={{
            ...dayOfWeekHeaderCell,
            gridColumnGap: 0,
            pl: 1,
            fontFamily: 'heading',
          }}
        >
          {day}
        </Box>
      ))}
    </Box>
  );
};

export default CalendarDays;
