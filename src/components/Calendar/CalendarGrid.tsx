'use client';

import { Box, Stack, Text } from '@chakra-ui/react';
import React from 'react';
import {
  dayGridItemContainer,
  dayGridItemContainerAndContentWrapper,
  daysOfWeekAndGrid,
} from './CalendarStyles';
import dayjs from 'dayjs';
import DayEvents from './DayEvents';
import { CalendarEvent, CalendarGridDayWithEvents } from '../../util/types';

interface CalendarGridProps {
  calendarDaysAndEvents: CalendarGridDayWithEvents[];
  data: CalendarEvent[];
}

const CalendarGrid: React.FC<CalendarGridProps> = ({
  calendarDaysAndEvents,
  data,
}) => {
  const previousOrNext = (day: any) => {
    if (day.isPreviousMonth || day.isNextMonth) {
      return 'red.500';
    }
    return 'textInfo';
  };

  const currentMonth = (day: any) => {
    if (day.isCurrentMonth) {
      return 'bold';
    }
    return 'normal';
  };

  const currentDay = (day: any) => {
    if (dayjs().isSame(day.dateString, 'day')) {
      return {
        backgroundColor: 'regularText',
        color: 'bg1',
        borderRadius: '50%',
        width: '1.5rem',
        height: '1.5rem',
        textAlign: 'center',
        ml: '-.1rem',
      };
    }
    return undefined;
  };

  return (
    <Box
      sx={{
        ...daysOfWeekAndGrid,
        borderTopColor: 'regularText',
        borderTopWidth: `2px`,
        borderTopStyle: 'solid',
      }}
    >
      {calendarDaysAndEvents.map((day: any) => (
        <Box
          sx={{
            ...dayGridItemContainer,
            color: previousOrNext(day),
            fontWeight: currentMonth(day),
          }}
          key={day.dateString}
        >
          <Box sx={dayGridItemContainerAndContentWrapper}>
            <Text
              sx={{
                ...currentDay(day),
                fontFamily: 'heading',
                fontSize: '1.1rem',
              }}
            >
              {day.dayOfMonth}
            </Text>
            <DayEvents events={day.events} dateString={day.dateString} />
          </Box>
        </Box>
      ))}
    </Box>
  );
};

export default CalendarGrid;
