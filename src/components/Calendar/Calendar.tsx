'use client';

import { Box } from '@chakra-ui/react';
import React, { useState } from 'react';
import CalendarHead from './CalendarHead';
import {
  createDaysForCurrentMonth,
  createDaysForNextMonth,
  createDaysForPreviousMonth,
  matchEventsToDays,
} from './calendarHelpers';
import { calendarRoot } from './CalendarStyles';
import CalendarDays from './CalendarDays';
import CalendarGrid from './CalendarGrid';
import {
  CalendarEvent,
  CalendarGridDay,
  SanityData,
  SanityIcs,
} from '../../util/types';
import EventList from '../EventList';
import CalendarInfo from './CalendarInfo';

interface CalendarProps {
  data: CalendarEvent[];
  icsFile: SanityIcs;
}

const Calendar: React.FC<CalendarProps> = ({ data, icsFile }) => {
  const [year, setYear] = useState(new Date().getFullYear());
  const [month, setMonth] = useState(new Date().getMonth() + 1);
  let currentMonthDays = createDaysForCurrentMonth(year, month);
  let previousMonthDays = createDaysForPreviousMonth(
    year,
    month,
    currentMonthDays
  );
  let nextMonthDays = createDaysForNextMonth(year, month, currentMonthDays);
  const calendarGridDayObjects: CalendarGridDay[] = [
    ...previousMonthDays,
    ...currentMonthDays,
    ...nextMonthDays,
  ];
  const calendarDaysAndEvents = matchEventsToDays(data, calendarGridDayObjects);
  const changeMonth = (month: number) => {
    if (month > 12) {
      setYear(year + 1);
      setMonth(1);
    } else if (month < 1) {
      setYear(year - 1);
      setMonth(12);
    } else {
      setMonth(month);
    }
  };

  const calendarWidgetStyle = {
    display: ['none', 'none', 'block'],
  };
  return (
    <Box sx={calendarRoot}>
      <CalendarInfo calendarEvents={data} calendarIcs={icsFile} />
      <CalendarHead
        onMonthChange={changeMonth}
        onYearChange={setYear}
        selectedYear={year}
        selectedMonth={month}
      />
      <Box sx={calendarWidgetStyle}>
        <CalendarDays />
      </Box>
      <Box sx={calendarWidgetStyle}>
        <CalendarGrid
          calendarDaysAndEvents={calendarDaysAndEvents}
          data={data}
        />
      </Box>
      <Box>
        <EventList
          calendarEvents={calendarDaysAndEvents}
          month={month}
          year={year}
        />
      </Box>
    </Box>
  );
};
export default Calendar;
