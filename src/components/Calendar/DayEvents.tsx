import {
  Box,
  Flex,
  Heading,
  IconButton,
  Link,
  Popover,
  PopoverContent,
  PopoverTrigger,
  Text,
} from '@chakra-ui/react';
import React from 'react';
import dayjs from 'dayjs';
import { FaInfoCircle } from '@react-icons/all-files/fa/FaInfoCircle';
import { FaExternalLinkSquareAlt } from '@react-icons/all-files/fa/FaExternalLinkSquareAlt';
import { CalendarGridDayWithEvents } from '../../util/types';

interface DayEventsProps {
  events: CalendarGridDayWithEvents[];
  dateString: string;
}

const DayEvents: React.FC<DayEventsProps> = ({ events, dateString }) => {
  const ignoreList = ['2024 Calendar of Events']; // TODO: fix this in scraper
  const eventsCleaned = events.filter((event: any) => {
    return !ignoreList.includes(event.title);
  });
  if (eventsCleaned.length === 0) {
    return null;
  }
  return (
    <Popover trigger="hover" computePositionOnMount isLazy preventOverflow>
      <PopoverTrigger>
        <Flex sx={{ alignItems: 'center', height: 'calc(100% - 2rem)' }}>
          <Text color="regularText">{`${eventsCleaned.length} events`}</Text>
          <IconButton
            color="textHighlight"
            aria-label="more information"
            icon={<FaInfoCircle />}
            sx={{
              background: 'none',
              ':hover': { background: 'none' },
            }}
          />
        </Flex>
      </PopoverTrigger>
      <PopoverContent
        border="none"
        sx={{
          borderRadius: 'lg',
          boxShadow: 'elevated',
          backgroundColor: 'bg1',
        }}
      >
        <Heading as="h4" size="md" sx={{ p: 2, color: 'textHighlight' }}>
          {dayjs(dateString, 'MM-DD-YYYY').format('MMM D, YYYY')}
        </Heading>
        {eventsCleaned.map((event: any) => (
          <Box
            sx={{
              p: 2,
              color: 'regularText',
              fontSize: '.9rem',
            }}
            key={`${event.startTime}${event.title.replace(/\s/g, '')}`}
          >
            <Link
              href={event.website}
              target="_blank"
              sx={{ fontWeight: 'bold', color: 'regularText' }}
            >
              <Flex alignItems={'center'} fontFamily="heading">
                {event.title}
                <FaExternalLinkSquareAlt style={{ marginLeft: '.5rem' }} />
              </Flex>
            </Link>
            <Text sx={{ fontWeight: 'normal', color: 'textInfo' }}>
              {dayjs(event.startTime).format('h:mm A')} -{' '}
              {dayjs(event.endTime).format('h:mm A')}
            </Text>
            <Text sx={{ fontWeight: 'normal', color: 'textInfo' }}>
              {event.location}
            </Text>
            <Text sx={{ fontWeight: 'normal', color: 'textInfo' }}>
              {event.address}
            </Text>
          </Box>
        ))}
      </PopoverContent>
    </Popover>
  );
};

export default DayEvents;
