import { Button, Flex, Select } from '@chakra-ui/react';
import React from 'react';
import { FaArrowLeft } from '@react-icons/all-files/fa/FaArrowLeft';
import { FaArrowRight } from '@react-icons/all-files/fa/FaArrowRight';
import { monthsArr, yearList } from './calendarHelpers';

interface CalendarHeadProps {
  onMonthChange: (month: number) => void;
  onYearChange: (month: number) => void;
  selectedMonth: number;
  selectedYear: number;
}

const CalendarHead: React.FC<CalendarHeadProps> = ({
  onMonthChange,
  onYearChange,
  selectedMonth,
  selectedYear,
}) => {
  return (
    <Flex flexWrap={'wrap'} justifyContent={'space-between'}>
      <Flex mr={8}>
        <Button
          leftIcon={<FaArrowLeft />}
          sx={{ mr: 2, borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }}
          onClick={() => onMonthChange(selectedMonth - 1)}
          variant="contrast"
        >
          Prev
        </Button>
        <Button
          rightIcon={<FaArrowRight />}
          onClick={() => onMonthChange(selectedMonth + 1)}
          sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }}
          variant="contrast"
        >
          Next
        </Button>
      </Flex>
      <Flex sx={{ mt: [2, 0] }}>
        <Select
          variant="outline"
          sx={{ mr: 2, width: 'auto' }}
          onChange={(e) => onMonthChange(parseInt(e.target.value))}
          value={selectedMonth}
          aria-label="Calendar month"
        >
          {monthsArr.map((month) => (
            <option key={`${month.label}-${month.value}`} value={month.value}>
              {month.label}
            </option>
          ))}
        </Select>
        <Select
          variant="outline"
          onChange={(e) => onYearChange(parseInt(e.target.value))}
          value={selectedYear}
          aria-label="Calendar year"
        >
          {yearList().map((year) => (
            <option key={`${year}`} value={year}>
              {year}
            </option>
          ))}
        </Select>
      </Flex>
    </Flex>
  );
};

export default CalendarHead;
