import {
  Flex,
  Heading,
  SystemStyleObject,
  useColorMode,
  useMediaQuery,
} from '@chakra-ui/react';
import React, { useMemo } from 'react';
// @ts-ignore
import BigLogoLight from '../images/logos/FallstownFestivities_logo_large_light.png';
// @ts-ignore
import BigLogoDark from '../images/logos/FallstownFestivities_logo_large_dark.png';
// @ts-ignore
import BigLogo800Light from '../images/logos/FallstownFestivities_logo_800_light.png';
// @ts-ignore
import BigLogo800Dark from '../images/logos/FallstownFestivities_logo_800_dark.png';
// @ts-ignore
import BigLogo600Light from '../images/logos/FallstownFestivities_logo_600_light.png';
// @ts-ignore
import BigLogo600Dark from '../images/logos/FallstownFestivities_logo_600_dark.png';
// @ts-ignore
import BigLogo400Light from '../images/logos/FallstownFestivities_logo_400_light.png';
// @ts-ignore
import BigLogo400Dark from '../images/logos/FallstownFestivities_logo_400_dark.png';
import SmartImage from './SmartImage';
import { maxWidth } from '../util/styles';

interface BrandingProps {
  sxProps?: SystemStyleObject;
}

const Branding: React.FC<BrandingProps> = ({ sxProps }) => {
  const { colorMode } = useColorMode();
  const [largerThan800] = useMediaQuery('(min-width: 800px)');
  const [largerThan600] = useMediaQuery('(min-width: 600px)');
  const [largerThan400] = useMediaQuery('(min-width: 400px)');

  const whichLogo = useMemo(() => {
    if (largerThan800) {
      const loaderSizes = { w: 352, h: 800 };
      return colorMode === 'dark'
        ? { src: BigLogoLight, loaderSizes }
        : { src: BigLogoDark, loaderSizes };
    } else if (largerThan600) {
      const loaderSizes = { w: 256, h: 600 };
      return colorMode === 'dark'
        ? { src: BigLogo800Light, loaderSizes }
        : { src: BigLogo800Dark, loaderSizes };
    } else if (largerThan400) {
      const loaderSizes = { w: 192, h: 400 };
      return colorMode === 'dark'
        ? { src: BigLogo600Light, loaderSizes }
        : { src: BigLogo600Dark, loaderSizes };
    } else {
      const loaderSizes = { w: 128, h: 300 };
      return colorMode === 'dark'
        ? { src: BigLogo400Light, loaderSizes }
        : { src: BigLogo400Dark, loaderSizes };
    }
  }, [colorMode, largerThan400, largerThan600, largerThan800]);

  return (
    <Flex
      sx={{
        width: '100%',
        maxWidth: maxWidth,
        mx: 4,
        flexDirection: 'column',
        alignItems: 'center',
        zIndex: 10,
        ...sxProps,
      }}
    >
      <SmartImage
        src={whichLogo.src}
        alt="Fallstown Festivities logo"
        loaderSize={whichLogo.loaderSizes}
        loading="eager"
        sxProps={{ ...sxProps, width: '100%' }}
      />
      <Heading as="h2" sx={{ mt: 4, fontSize: largerThan400 ? '4xl' : '2xl' }}>
        Fallstown Festivities
      </Heading>
    </Flex>
  );
};

export default Branding;
