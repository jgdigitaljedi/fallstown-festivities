import {
  Box,
  Flex,
  Icon,
  Image,
  Link,
  Stack,
  Text,
  useColorMode,
} from '@chakra-ui/react';
import React from 'react';
import { FaRegCopyright } from '@react-icons/all-files/fa/FaRegCopyright';
// @ts-ignore
import LogoLight from '../images/logos/FallstownFestivities_logo_128_light.png';
// @ts-ignore
import LogoDark from '../images/logos/FallstownFestivities_logo_128_dark.png';
import { FaEye } from '@react-icons/all-files/fa/FaEye';
import { FaSitemap } from '@react-icons/all-files/fa/FaSitemap';
import { FaPhone } from '@react-icons/all-files/fa/FaPhone';

const Footer: React.FC = () => {
  const { colorMode } = useColorMode();
  const footerLinks = [
    {
      name: 'Contact',
      icon: FaPhone,
      href: '/contact',
    },
    {
      name: 'Privacy Policy',
      icon: FaEye,
      href: '/privacy-policy',
    },
    {
      name: 'Sitemap',
      icon: FaSitemap,
      href: '/sitemap-0.xml',
    },
  ];
  return (
    <Box
      as="footer"
      sx={{
        bottom: 0,
        mt: 8,
        backgroundColor: 'bg1',
        pt: 10,
        width: '100%',
        py: 8,
        boxShadow: 'top',
        overflowX: 'hidden',
        content: "''",
        backgroundImage: `repeating-radial-gradient(bg1 0% 12%,bg3 13% 26% ), linear-gradient(to top, rgba(0, 0, 0, .1), rgba(0, 0, 0, 0))`,
        backgroundSize: '100px 100px',
      }}
    >
      <Stack
        sx={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          opacity: 1,
          maskImage: 'none',
        }}
      >
        <Box
          sx={{
            lineHeight: 0.5,
            textAlign: 'center',
            mb: 1,
          }}
        >
          <Box
            as="span"
            sx={{
              display: 'inline-block',
              position: 'relative',
              '::before, ::after': {
                content: "''",
                position: 'absolute',
                height: '5px',
                top: 0,
                width: ['50px', '100px', '200px', '300px'],
                borderTopColor: 'regularText',
                borderBottomColor: 'regularText',
                borderTopWidth: '1px',
                borderBottomWidth: '1px',
                borderTopStyle: 'solid',
                borderBottomStyle: 'solid',
              },
              '::before': {
                right: 0,
                marginRight: 'calc(100% + 1rem)',
              },
              '::after': {
                left: 0,
                marginLeft: 'calc(100% + 1rem)',
              },
            }}
          >
            social sharing buttons here
          </Box>
        </Box>
        <Flex
          sx={{
            justifyContent: 'center',
            alignItems: 'center',
            mask: 'none',
          }}
        >
          <Text mr={2}>Copyright</Text>
          <FaRegCopyright style={{ marginRight: '.5rem' }} />
          2024 Joey Gauthier
        </Flex>
        <Box>
          <Image
            src={colorMode === 'dark' ? LogoLight : LogoDark}
            alt="Fallstown Festivities logo"
          />
        </Box>
        <Flex
          sx={{
            width: '100%',
            maxWidth: '500px',
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: ['column', 'column', 'row'],
          }}
        >
          {footerLinks.map((link) => (
            <Link
              key={link.name}
              href={link.href}
              sx={{
                display: 'flex',
                alignItems: 'center',
                my: [2, 2, 0],
              }}
            >
              <Icon as={link.icon} marginRight={2} /> {link.name}
            </Link>
          ))}
        </Flex>
      </Stack>
    </Box>
  );
};

export default Footer;
