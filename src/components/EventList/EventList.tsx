import React, { useMemo } from 'react';
import { CalendarGridDayWithEvents } from '../../util/types';
import dayjs from 'dayjs';
import { Flex, Heading, Stack } from '@chakra-ui/react';
import EventCard from './EventCard';

interface EventListProps {
  calendarEvents: CalendarGridDayWithEvents[];
  month: number;
  year: number;
}

const EventList: React.FC<EventListProps> = ({
  calendarEvents,
  month,
  year,
}) => {
  const upcomingEvents = useMemo(
    () =>
      calendarEvents
        .filter((day) => {
          if (day.isNextMonth) {
            return true;
          }
          const dateObj = dayjs(day.dateString, 'MM-DD-YYYY');
          return (
            dateObj.isSameOrAfter(dayjs(), 'day') &&
            dateObj.month() >= month - 1 &&
            dateObj.year() === year
          );
        })
        .filter((day) => day.events.length > 0),
    [calendarEvents, month, year]
  );

  if (upcomingEvents.length === 0) {
    return (
      <Flex sx={{ justifyContent: 'center', py: 6, width: '100%' }}>
        <Heading as="h3" size="lg">
          There were no events found for this month.
        </Heading>
      </Flex>
    );
  }

  return (
    <Stack>
      {upcomingEvents.map((day) => (
        <EventCard key={day.dateString} day={day} />
      ))}
    </Stack>
  );
};

export default EventList;
