import React from 'react';
import { CalendarGridDayWithEvents } from '../../util/types';
import {
  Box,
  Card,
  CardBody,
  CardHeader,
  Text,
  Heading,
  Link,
  Flex,
  Divider,
  Tooltip,
  Icon,
} from '@chakra-ui/react';
import dayjs from 'dayjs';
import { FaExternalLinkSquareAlt } from '@react-icons/all-files/fa/FaExternalLinkSquareAlt';
import { FaMap } from '@react-icons/all-files/fa/FaMap';
import { FaPhone } from '@react-icons/all-files/fa/FaPhone';
// import { FaInfoCircle } from '@react-icons/all-files/fa/FaInfoCircle';
import { FaExpandAlt } from '@react-icons/all-files/fa/FaExpandAlt';

interface EventCardProps {
  day: CalendarGridDayWithEvents;
}

const EventCard: React.FC<EventCardProps> = ({ day }) => {
  const getKey = (event: any) => {
    return `${event.title}${event.startTime}`.replace(/\s/g, '');
  };

  const getLocationLine = (event: any) => {
    if (event.location && event.address) {
      return `${event.location} - ${event.address}`;
    } else if (event.location) {
      return event.location;
    } else if (event.address) {
      return event.address;
    } else {
      return '';
    }
  };

  const getAddressLine = (event: any) => {
    if (event.geo?.lat && event.geo?.lon && (event.address || event.location)) {
      return (
        <Box>
          <Flex alignItems={'center'}>
            <Link
              href={`geo:${event.geo.lat},${event.geo.lon}`}
              sx={{
                display: 'flex',
                alignItems: 'center',
                fontStyle: 'italic',
                mr: 2,
                mb: '.2rem',
              }}
              target="_blank"
            >
              <FaMap style={{ marginRight: '1rem' }} />
              {getLocationLine(event)}
            </Link>
          </Flex>
        </Box>
      );
    } else if (event.address || event.location) {
      return (
        <Box>
          <Flex alignItems={'center'} marginBottom=".2rem">
            <Text>{getLocationLine(event)}</Text>
          </Flex>
        </Box>
      );
    }
    return null;
  };

  const truncateDescription = (text: string): string => {
    return `${text.slice(0, 100)}${text.length > 100 ? '...' : ''}`;
  };

  return (
    <Card sx={{ mt: 6, boxShadow: 'card', backgroundColor: 'bg1' }}>
      <CardHeader color="regularText" paddingBottom={0}>
        <Heading as="h3" size="lg" marginBottom=".2rem">
          {dayjs(day.dateString, 'MM-DD-YYYY').format('dddd, MMMM D, YYYY')}
        </Heading>
      </CardHeader>
      <CardBody color="textNext">
        {day.events.map((event) => (
          <Box key={getKey(event)} sx={{ mt: 4 }}>
            <Divider marginBottom={2} />
            <Heading as="h4" size="md" color="textHighlight">
              {event.title}
            </Heading>
            <Text fontWeight="bold" marginBottom={1}>
              {event.dateTime}
            </Text>
            {getAddressLine(event)}
            {!!event.phone && (
              <Link
                href={`tel:${event.phone.replace(/\D/g, '')}`}
                sx={{ display: 'flex', alignItems: 'center', mb: '.2rem' }}
              >
                <FaPhone style={{ marginRight: '1rem' }} />
                {event.phone}
              </Link>
            )}
            <Text fontStyle={'italic'} marginBottom=".2rem">
              {event.categories.join(', ')}
            </Text>
            <Text
              sx={{
                color: 'textInfo',
                mb: '.2rem',
                display: ['none', 'none', 'block'],
              }}
            >
              {event.description}
            </Text>
            <Box
              sx={{
                color: 'textInfo',
                mb: '.2rem',
                display: ['block', 'block', 'none'],
              }}
            >
              <Tooltip
                label={event.description}
                sx={{ p: 2, boxShadow: 'card' }}
              >
                <Text sx={{ display: 'inline-block' }}>
                  {truncateDescription(event.description)}
                  {event.description.length > 100 && (
                    <Icon
                      as={FaExpandAlt}
                      sx={{
                        display: 'inline',
                        ml: 1,
                        color: 'textHighlight',
                        cursor: 'pointer',
                      }}
                    />
                  )}
                </Text>
              </Tooltip>
            </Box>
            <Link
              href={
                event.eventWebsite ||
                event.eventsWebsite ||
                event.website ||
                undefined
              }
              target="_blank"
              sx={{ display: 'flex', alignItems: 'center' }}
            >
              <Text marginRight={3}>Event website</Text>
              <FaExternalLinkSquareAlt />
            </Link>
          </Box>
        ))}
      </CardBody>
    </Card>
  );
};

export default EventCard;
