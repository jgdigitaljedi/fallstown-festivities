import { HeadFC, PageProps } from 'gatsby';
import React from 'react';
import Layout from '../templates/Layout';
import { Box } from '@chakra-ui/react';
import { SEO } from '../templates/Seo';

const pageDesc = 'Your source for information about Wichita Falls!';
const pageTitle = 'Info';

const Info: React.FC<PageProps> = ({ location }) => {
  return (
    <Layout location={location}>
      <Box as="main">Info</Box>
    </Layout>
  );
};

export default Info;

export const Head: HeadFC = () => (
  <SEO title={pageTitle} description={pageDesc} pathname="/info">
    <title id="page-title">{pageTitle}</title>
    <meta name="description" id="description" content={pageDesc} />
  </SEO>
);
