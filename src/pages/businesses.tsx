import {
  Box,
  Flex,
  Heading,
  Stack,
  TabPanels,
  Tabs,
  Text,
} from '@chakra-ui/react';
import React, { useState } from 'react';
import Layout from '../templates/Layout';
import { HeadFC, PageProps, graphql } from 'gatsby';
import { SEO } from '../templates/Seo';
import { maxWidth } from '../util/styles';
import { dataForType, getBusinessTabs } from '../util/businessHelpers';
import BusinessTabs from '../components/Business/BusinessTabs';
import BusinessPanel from '../components/Business/BusinessPanel';
import SmartImage from '../components/SmartImage';
import FiltersWrapper from '../components/Business/BusinessFilters/FiltersWrapper';

const pageDesc = 'Discover restuarants and businesses in Wichita Falls!';
const pageTitle = 'Businesses';

interface BusinessesProps extends PageProps {
  data: {
    allSanityBusiness: {
      nodes: any[];
    };
  };
}

const Businesses: React.FC<BusinessesProps> = ({ location, data }) => {
  const nodes = data.allSanityBusiness.nodes;
  const [activeTab, setActiveTab] = useState('restaurant');
  const [currentData, setCurrentData] = useState(
    dataForType('restaurant', nodes)
  );
  const [filteredData, setFilteredData] = useState(
    dataForType('restaurant', nodes)
  );
  console.log('businesses data', currentData);
  const tabs = getBusinessTabs(nodes);
  const splitData = tabs
    .map((tab) => {
      const tabData = dataForType(tab.toLowerCase(), nodes);
      return { tab, tabData };
    })
    .reduce((acc, cur) => {
      return {
        ...acc,
        [cur.tab]: cur.tabData,
      };
    }, {});

  const onTabChange = (tab: string) => {
    setActiveTab(tab);
    setCurrentData(splitData[tab as keyof typeof splitData]);
  };

  const onFiltersChange = (data: any) => {
    console.log('onFIltersChange', data);
    setFilteredData(data);
  };

  return (
    <Layout location={location}>
      <Box
        as="main"
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          width: '100%',
          flexDirection: 'column',
        }}
      >
        <Stack sx={{ width: '100%', maxWidth: maxWidth, alignItems: 'center' }}>
          <Heading as="h1" size="2xl">
            Businesses
          </Heading>
          <Flex
            sx={{
              my: 8,
              textAlign: 'center',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            <Box sx={{ zIndex: 5 }}>
              <SmartImage
                src="https://placehold.co/800x600"
                alt="a placeholder for now"
                loaderSize={{ w: 800, h: 600 }}
                loading="eager"
                sxProps={{ width: '100%', height: 'auto' }}
              />
            </Box>
            <Text sx={{ fontSize: '2rem', mt: 4 }}>
              Not sure where to eat or what to do tonight? This is your one stop
              shop to find new places to go around Wichita Falls to have a good
              time!
            </Text>
          </Flex>
        </Stack>
        <Box sx={{ width: '100%', maxWidth }}>
          <FiltersWrapper
            activeTab={activeTab}
            businesses={currentData}
            onFiltersChange={onFiltersChange}
          />
        </Box>
        <Tabs
          isFitted
          variant="enclosed"
          onChange={(index: number) => {
            onTabChange(tabs[index]);
          }}
        >
          <BusinessTabs tabs={tabs} />
          <TabPanels>
            {tabs.map((tab) => (
              <BusinessPanel
                key={`${tab}-panel`}
                data={filteredData}
                tab={tab}
              />
            ))}
          </TabPanels>
        </Tabs>
      </Box>
    </Layout>
  );
};

export default Businesses;

export const Head: HeadFC = () => (
  <SEO title={pageTitle} description={pageDesc} pathname="/businesses">
    <title id="page-title">{pageTitle}</title>
    <meta name="description" id="description" content={pageDesc} />
  </SEO>
);

export const query = graphql`
  query BusinessesQuery {
    allSanityBusiness {
      nodes {
        address
        name
        id
        imageUrl
        yelpUrl
        categories
        rating
        reviewCount
        city
        coordinates {
          lat
          lon
        }
        transactions
        price
        location
        phone
        displayPhone
        hours {
          day
          breakdown {
            start
            end
            startSplit
            endSplit
          }
          display
        }
        menuUrl
        type
      }
    }
  }
`;
