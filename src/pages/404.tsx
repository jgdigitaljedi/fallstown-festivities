import * as React from 'react';
import { Link, HeadFC, PageProps } from 'gatsby';
import { Box, Flex, Heading } from '@chakra-ui/react';
import { SEO } from '../templates/Seo';
import Layout from '../templates/Layout';
import { FaExclamationTriangle } from '@react-icons/all-files/fa/FaExclamationTriangle';

const paragraphStyles = {
  marginBottom: 48,
};

const pageDesc = 'You have hit a page that does not exist.';
const pageTitle = 'Page not found';

const NotFoundPage: React.FC<PageProps> = ({ location }) => {
  return (
    <Layout location={location}>
      <Box as="main" sx={{ textAlign: 'center' }}>
        <Flex sx={{ justifyContent: 'center', alignItems: 'baseline' }}>
          <FaExclamationTriangle
            style={{
              marginRight: '1rem',
              fontSize: '2rem',
            }}
          />
          <Heading as="h1" size="xl" marginBottom={6}>
            Page not found
          </Heading>
          <FaExclamationTriangle
            style={{ marginLeft: '1rem', fontSize: '2rem' }}
          />
        </Flex>
        <p style={paragraphStyles}>
          Sorry 😔, we couldn’t find what you were looking for. Use the
          navigation at the top of this page to view an existing page.
        </p>
      </Box>
    </Layout>
  );
};

export default NotFoundPage;

export const Head: HeadFC = () => (
  <SEO title={pageTitle} description={pageDesc} pathname="/404">
    <title id="page-title">{pageTitle}</title>
    <meta name="description" id="description" content={pageDesc} />
  </SEO>
);
