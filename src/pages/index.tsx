import * as React from 'react';
import type { HeadFC, PageProps } from 'gatsby';
import Layout from '../templates/Layout';
import {
  Box,
  Flex,
  Text,
  Image,
  Grid,
  Stack,
  GridItem,
  useBreakpointValue,
} from '@chakra-ui/react';
import Branding from '../components/Branding';
import { SEO } from '../templates/Seo';

const pageDesc = 'Your place to find all things Wichita Falls!';
const pageTitle = 'Fallstown Festivities';

interface ImageAndTextProps {
  text: string;
  image: string;
  altText: string;
  reverse?: boolean;
}

const IndexPage: React.FC<PageProps> = ({ location }) => {
  const triangleHeight = useBreakpointValue({
    base: '30vh',
    sm: '45vh',
    md: '50vh',
    lg: '55vh',
  });
  const imageAndTextArr = [
    {
      text: 'Learn about events happening in the Wichita Falls area and add them to your calendar.',
      image: 'https://placehold.co/800x600',
      altText: 'placehoder',
    },
    {
      text: 'Find new places to eat or have a drink.',
      image: 'https://placehold.co/800x600',
      altText: 'placehoder',
      reverse: true,
    },
    {
      text: 'Discover other types of activities availble in the area.',
      image: 'https://placehold.co/800x600',
      altText: 'placehoder',
    },
  ];
  const imageAndTextGen = (
    text: string,
    image: string,
    altText: string,
    reverse?: boolean
  ): React.ReactElement => {
    return (
      <Flex
        sx={{
          flexDirection: reverse
            ? ['column', 'column', 'row-reverse']
            : ['column', 'column', 'row'],
          my: 8,
        }}
      >
        <Flex
          sx={{
            justifyContent: 'center',
            alignItems: 'center',
            maxWidth: ['100%', '100%', '25rem'],
            px: 4,
          }}
        >
          <Text
            sx={{ fontSize: '2rem', textAlign: 'center', my: [6, 6, 'auto'] }}
          >
            {text}
          </Text>
        </Flex>
        <Box>
          <Image src={image} alt={altText} loading="lazy" />
        </Box>
      </Flex>
    );
  };

  return (
    <Layout location={location} noPadding={true}>
      <Box as="main" sx={{ overflowX: 'hidden' }}>
        <Grid templateColumns={'1fr'}>
          <GridItem>
            <Box sx={{ position: 'relative', zIndex: 3 }}>
              <Box
                sx={{
                  width: '50%',
                  height: triangleHeight,
                  backgroundColor: 'bg2',
                  backgroundSize: 'cover',
                  clipPath: 'polygon(0% 100%, 100% 100%, 0% 0%)',
                  position: 'absolute',
                  zIndex: -1,
                  backgroundImage:
                    'radial-gradient(farthest-corner at 10% 10%, bg2 0%, bg1 100%)',
                }}
              ></Box>
              <Box
                sx={{
                  width: '50%',
                  height: triangleHeight,
                  backgroundColor: 'bg2',
                  backgroundSize: 'cover',
                  clipPath: 'polygon(100% 0%, 0% 100%, 100% 100%)',
                  position: 'absolute',
                  right: 0,
                  zIndex: -1,
                  backgroundImage:
                    'radial-gradient(farthest-corner at 10% 90%, bg1 0%, bg2 100%)',
                }}
              ></Box>
            </Box>
            <Stack
              sx={{
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                p: 6,
              }}
            >
              <Branding sxProps={{ width: '100%', mb: 6, mt: 6 }} />
              <Text
                sx={{
                  fontSize: ['2re,', '2rem', '2.5rem', '3rem'],
                  maxWidth: '60rem',
                  textAlign: 'center',
                  zIndex: 5,
                }}
              >
                Your place to find what to do, where to go, and more in the
                Wichita Falls area!
              </Text>
            </Stack>
          </GridItem>
          <GridItem sx={{ zIndex: 5 }}>
            <Box sx={{ height: '50%' }}>
              <Image
                src="https://placehold.co/3800x1200"
                alt="placeholder"
                sx={{ objectFit: 'stretch' }}
                loading="eager"
              />
            </Box>
          </GridItem>
        </Grid>
        <Stack sx={{ justifyContent: 'center', alignItems: 'center' }}>
          {imageAndTextArr.map((imageAndText: ImageAndTextProps) =>
            imageAndTextGen(
              imageAndText.text,
              imageAndText.image,
              imageAndText.altText,
              imageAndText.reverse
            )
          )}
        </Stack>
      </Box>
    </Layout>
  );
};

export default IndexPage;

export const Head: HeadFC = () => (
  <SEO title={pageTitle} description={pageDesc} pathname="/">
    <title id="page-title">{pageTitle}</title>
    <meta name="description" id="description" content={pageDesc} />
  </SEO>
);
