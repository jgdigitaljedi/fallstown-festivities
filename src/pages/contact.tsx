import { HeadFC, PageProps } from 'gatsby';
import React from 'react';
import Layout from '../templates/Layout';
import { Box } from '@chakra-ui/react';
import { SEO } from '../templates/Seo';

const pageDesc = 'Contact Joey about the site.';
const pageTitle = 'Contact me';

const Contact: React.FC<PageProps> = ({ location }) => {
  return (
    <Layout location={location}>
      <Box as="main">Contact me</Box>
    </Layout>
  );
};

export default Contact;

export const Head: HeadFC = () => (
  <SEO title={pageTitle} description={pageDesc} pathname="/privacy-policy">
    <title id="page-title">{pageTitle}</title>
    <meta name="description" id="description" content={pageDesc} />
  </SEO>
);
