import { Box, Flex, Text } from '@chakra-ui/react';
import React from 'react';
import Layout from '../templates/Layout';
import { graphql, HeadFC, PageProps } from 'gatsby';
import { SEO } from '../templates/Seo';
import SmartImage from '../components/SmartImage';
import { CalendarEvent, SanityIcs } from '../util/types';
import Calendar from '../components/Calendar';
import { maxWidth } from '../util/styles';

const pageDesc = 'Find things to do in Wichita Falls!';
const pageTitle = 'Festivities';

interface FestivitiesProps extends PageProps {
  data: {
    allSanityEvent: {
      nodes: CalendarEvent[];
    };
    allSanityFileAsset: {
      nodes: SanityIcs[];
    };
  };
}

const Festivities: React.FC<FestivitiesProps> = ({ location, data }) => {
  return (
    <Layout location={location} innerSxProps={{ px: [2, 2, 8] }}>
      <Box
        as="main"
        sx={{
          maxWidth: maxWidth,
          mx: 'auto',
        }}
      >
        <Flex
          sx={{
            my: 8,
            textAlign: 'center',
            alignItems: 'center',
            flexDirection: 'column',
          }}
        >
          <Box sx={{ zIndex: 5 }}>
            <SmartImage
              src="https://placehold.co/800x600"
              alt="a placeholder for now"
              loaderSize={{ w: 800, h: 600 }}
              loading="eager"
              sxProps={{ width: '100%', height: 'auto' }}
            />
          </Box>
          <Text sx={{ fontSize: '2rem', mt: 4 }}>
            See what is happening around town. There's probably more going on
            that you were aware of, and it is all right here! You can download
            the calendar file with all events as well.
          </Text>
        </Flex>
        <Box sx={{ minHeight: '800px' }}>
          <Calendar
            data={data?.allSanityEvent?.nodes}
            icsFile={data?.allSanityFileAsset?.nodes[1]}
          />
        </Box>
      </Box>
    </Layout>
  );
};

export default Festivities;

export const Head: HeadFC = () => (
  <SEO title={pageTitle} description={pageDesc} pathname="/festivities">
    <title id="page-title">{pageTitle}</title>
    <meta name="description" id="description" content={pageDesc} />
  </SEO>
);

export const query = graphql`
  query EventsQuery {
    allSanityEvent {
      nodes {
        address
        calendar
        categories
        dateTime
        description
        endTime
        eventWebsite
        eventsWebsite
        geo {
          lat
          lon
        }
        location
        name
        phone
        startTime
        title
        website
      }
    }
    allSanityFileAsset {
      nodes {
        mimeType
        originalFilename
        url
        _updatedAt
      }
    }
  }
`;
