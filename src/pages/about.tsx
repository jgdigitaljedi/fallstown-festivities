import {
  Box,
  Flex,
  Grid,
  Heading,
  Icon,
  Link,
  Stack,
  Text,
} from '@chakra-ui/react';
import React from 'react';
import Layout from '../templates/Layout';
import { HeadFC, PageProps } from 'gatsby';
import { SEO } from '../templates/Seo';
import { FaReact } from '@react-icons/all-files/fa/FaReact';
import { RiGatsbyFill } from '@react-icons/all-files/ri/RiGatsbyFill';
import { FaNodeJs } from '@react-icons/all-files/fa/FaNodeJs';
import { FaExternalLinkSquareAlt } from '@react-icons/all-files/fa/FaExternalLinkSquareAlt';
import Branding from '../components/Branding';
import SmartImage from '../components/SmartImage';
import { FaCode } from '@react-icons/all-files/fa/FaCode';
// @ts-ignore
import MeAtWurstfest from '../images/MeAtWurstfestCropped.jpg';
import { maxWidth } from '../util/styles';

const pageDesc = 'Information about Fallstown Festivities';
const pageTitle = 'About';

const About: React.FC<PageProps> = ({ location }) => {
  const textSectionStyles = {
    my: 6,
    maxWidth: maxWidth,
    borderRadius: 'lg',
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: 'textHighlight',
    p: 6,
    backgroundColor: 'bg1',
    boxShadow: 'elevated',
  };
  const rowStyles = {
    justifyItems: 'center',
    alignItems: 'center',
    width: '100%',
    mb: 4,
  };
  const headingStyles = {
    mb: 6,
    display: 'flex',
    justifyContent: 'center',
    color: 'textHighlight2',
    borderBottomStyle: ['none', 'none', 'solid'],
    borderBottomWidth: '1px',
    borderBottomColor: 'textInfo',
  };
  const blurbStyles = { fontSize: '1.2rem' };
  const siteTech = [
    {
      name: 'React',
      link: 'https://react.dev/',
      icon: FaReact,
      description:
        'React is a JavaScript library for building user interfaces. This is the technology I am using to simplify the visual layer and state management for the site.',
    },
    {
      name: 'Gatsby',
      link: 'https://www.gatsbyjs.com/',
      icon: RiGatsbyFill,
      description:
        'Gatsby is a React-based framework for static site generation. This is the technology I am using to make this site static which provides better SEO and seach engine optimization.',
    },
    {
      name: 'NodeJS',
      link: 'https://nodejs.org/en',
      icon: FaNodeJs,
      description:
        "NodeJS is a JavaScript runtime built on Chrome's V8 JavaScript engine. This is the technology I am using to run a development environment, generate that static site builds to be deployed, and to gather the events data from various other websites.",
    },
    {
      name: 'Chakra UI',
      link: 'https://v2.chakra-ui.com/',
      icon: null,
      description:
        'Chakra UI is a UI toolkit for building user interfaces. This is the technology I am using to speed up development and provide the visual styling for the site.',
    },
    {
      name: 'Sanity.io',
      link: 'https://www.sanity.io/',
      icon: null,
      description:
        'Sanity.io is a headless CMS that allows me to easily generate and manage static websites. This is the technology I am using to write my events and business data to every day so it can be pulled into the static generation of the site each morning.',
    },
    {
      name: 'Netlify',
      link: 'https://www.netlify.com/',
      icon: null,
      description:
        'Netlify is a cloud hosting platform. This is the technology I am using to host the site and deploy it.',
    },
  ];

  const siteTips = [
    `There is a toggle in the top right corner of the page to switch the site’s theme from light to dark and back. The site will remember what you used last so you can somewhat customize your experience a little bit. I developed this as “dark mode” first as I prefer dark modes and think it is easier on the eyes so I would argue that you should try dark mode if you haven’t yet. That said, use whichever mode you wish as I’m just happy you’re here!`,
    `The site rebuilds itself one a day in the very early morning. That said, you can check back daily for the latest information available. I have a series of web scrapers and crawlers running to gather the information for me that is making this all happen automatically so even if I leave for a vacation, the site should still update itself!`,
    `I created the site to be completely responsive so the layout changes for different sized screens. I also built it as a progressive web app meaning you can add it to the home screen of your phone like a regular app and it will store the data on your phone to be used offline as well. If you wish to “install” this as an app on your phone, simply look up how to install a progressive web app for your mobile operating system (iPhone or Android).`,
  ];
  return (
    <Layout location={location} innerSxProps={{ px: [2, 2, 8] }}>
      <Stack
        as="main"
        sx={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}
      >
        <Stack
          sx={{
            maxWidth: ['100%', '100%', '100%', '80rem'],
            width: '100%',
            alignItems: 'center',
          }}
        >
          <Stack mb={4}>
            <Branding />
            <Box
              as="p"
              sx={{
                maxWidth: '60rem',
                textAlign: 'center',
                mt: 2,
                fontSize: '1.5rem',
              }}
            >
              This site aims to better inform the folks of the Wichita Falls
              area of events, city info, and more while providing basic
              information regarding area businesses and charities. The idea is
              to be your one stop shop for all things Wichita Falls!
            </Box>
          </Stack>
          <Box sx={textSectionStyles}>
            <Flex width="100%" justifyContent={'center'}>
              <Heading as="h2" size={['lg', 'lg', 'xl']} sx={headingStyles}>
                About the author
              </Heading>
            </Flex>
            <Grid
              templateColumns={{ base: '1fr', md: '1fr 1fr' }}
              gap={6}
              sx={rowStyles}
            >
              <Box as="p" sx={blurbStyles}>
                My name is Joey, and I grew up in the Wichita Falls area. After
                moving away a handful of times to pursue a career as a musician
                and again to grow my career in tech, I have come back to Wichita
                Falls to be near family and live life at a little slower pace. I
                work remotely as a senior software engineer for a large tech
                company, and I have decided to use some of my skill set and free
                time to create this site for the people of Wichita Falls. I love
                this city and wish to see it thrive!
              </Box>
              <SmartImage
                src={MeAtWurstfest}
                alt="Me at Wurstfest"
                loaderSize={{ w: 300, h: 200 }}
                loading="eager"
                sxProps={{ gridColumn: [1, 1, 2], gridRow: 1 }}
              />
            </Grid>
          </Box>
          <Box sx={textSectionStyles}>
            <Flex width="100%" justifyContent={'center'}>
              <Heading as="h2" size={['lg', 'lg', 'xl']} sx={headingStyles}>
                My motivation for creating this site
              </Heading>
            </Flex>
            <Grid
              templateColumns={{ base: '1fr', md: '1fr 1fr' }}
              gap={6}
              sx={rowStyles}
            >
              <SmartImage
                src="https://placehold.co/400x400"
                alt="a placeholder for now"
                loaderSize={{ w: 400, h: 400 }}
                loading="eager"
                sxProps={{ width: '100%' }}
              />
              <Box as="p" sx={blurbStyles}>
                Once I moved back, I began poking around online for things to do
                and I realized that this city has a lot to offer, but the
                awareness and discoverability of those activities is lacking
                severely. After a few months of visiting the same handful of
                restaurants and occasionally patronizing the same bar, I
                realized a need to have one site where we could all go to find
                information available regarding what to do, where to go, etc
                around here.
                <Box as="p" sx={{ ...blurbStyles, mt: 4 }}>
                  I thought it made sense to create a site containing as much
                  information as possible regarding the many forms of
                  entertainment around here. This way, everyone would have one
                  place to go to help them decide how to spend their weekend in
                  town, where to eat tonight, what you can do with the kids this
                  weekend for free, etc.
                </Box>
              </Box>
            </Grid>
          </Box>
          <Box sx={textSectionStyles}>
            <Flex width="100%" justifyContent={'center'}>
              <Heading as="h2" size={['lg', 'lg', 'xl']} sx={headingStyles}>
                My plans for the site
              </Heading>
            </Flex>
            <Box as="p" sx={{ ...blurbStyles, mb: 2 }}>
              I view this site as a long term project for which I will
              continuously build new features. Some of the plans I currently
              have include:
            </Box>
            <Grid
              templateColumns={{ base: '1fr', md: '1fr 1fr' }}
              gap={6}
              sx={rowStyles}
            >
              <Box as="ul" sx={{ px: 2, pl: 8, ...blurbStyles }}>
                <Box as="li">
                  A "Businesses" page that will include restaurants, bars, and
                  places of entertainment like movie theaters, bowling alleys,
                  etc. You will be able to search and filter these businesses by
                  category and even have the site randomly suggest a place for
                  you!
                </Box>
                <Box as="li">
                  An "Info" page that will include a list of free activities,
                  lake levels, watering and trash schedules, information
                  regarding where to pay utlity bills, etc.
                </Box>
                <Box as="li">
                  A rants page and a marketplace page. As of right now, most
                  folks appear to use Facebook for both of these things, but
                  there are folks out there such as myself that do not use
                  Facebook so creating pages for these things would result in
                  more people having access to them.
                </Box>
                <Box as="li">
                  Ad spaces and sponsorships. Yeah, yeah, I know most people
                  hate those things. This site has a monetary cost associated
                  with keeping it online and I am spending a large amount of my
                  free time creating and maintaining it. Eventually creating a
                  way for the site to pay for itself is a requirement.
                </Box>
              </Box>
              <SmartImage
                src="https://placehold.co/400x400"
                alt="a placeholder for now"
                loaderSize={{ w: 400, h: 400 }}
                loading="eager"
                sxProps={{ width: '100%', gridColumn: [1, 1, 2], gridRow: 1 }}
              />
            </Grid>
          </Box>
          <Box sx={textSectionStyles}>
            <Flex width="100%" justifyContent={'center'}>
              <Heading as="h2" size={['lg', 'lg', 'xl']} sx={headingStyles}>
                Some tips about the site
              </Heading>
            </Flex>
            <Grid
              templateColumns={{ base: '1fr', md: '1fr 1fr' }}
              gap={6}
              sx={rowStyles}
            >
              <SmartImage
                src="https://placehold.co/400x400"
                alt="a placeholder for now"
                loaderSize={{ w: 400, h: 400 }}
                loading="eager"
                sxProps={{ width: '100%' }}
              />
              <Box sx={blurbStyles}>
                <Box as="ul" sx={{ px: 2, ml: 8 }}>
                  {siteTips.map((tip) => (
                    <Box as="li" my={3} key={tip.slice(0, 10)}>
                      {tip}
                    </Box>
                  ))}
                </Box>
              </Box>
            </Grid>
          </Box>
          <Box sx={textSectionStyles}>
            <Flex
              alignItems={'center'}
              justifyContent={'center'}
              marginBottom={4}
            >
              <Icon
                sx={{
                  fontSize: '2rem',
                  marginRight: '1rem',
                  color: 'textHighlight2',
                }}
                as={FaCode}
              />
              <Heading
                as="h2"
                size={['lg', 'lg', 'xl']}
                sx={{ ...headingStyles, pb: 0, mb: 0 }}
              >
                For anyone curious about how this site was built
              </Heading>
              <Icon
                sx={{
                  fontSize: '2rem',
                  marginLeft: '1rem',
                  color: 'textHighlight2',
                }}
                as={FaCode}
              />
            </Flex>
            <Box as="p" sx={blurbStyles}>
              This site was built using the following technologies:
              <Box as="ul" ml={8}>
                {siteTech.map((tech) => (
                  <Box
                    as="li"
                    sx={{ maxWidth: '60rem', my: 3 }}
                    key={tech.name}
                  >
                    <Link
                      href={tech.link}
                      target="_blank"
                      sx={{
                        display: 'flex',
                        alignItems: 'baseline',
                        fontweight: 'bold',
                        fontFamily: 'heading',
                        color: 'textHighlight',
                      }}
                      isExternal
                      title={`Open ${tech.name} link in a new tab`}
                    >
                      {tech.icon ? <Icon as={tech.icon} mr={1} /> : null}
                      <Text mr={2}>{tech.name}</Text>
                      <FaExternalLinkSquareAlt style={{ fontSize: '.75rem' }} />
                    </Link>
                    <Text>{tech.description}</Text>
                  </Box>
                ))}
              </Box>
            </Box>
            <Box as="p" sx={blurbStyles}>
              The site rebuilds daily to keep the information fresh. At 3am
              every day, the calendar/events scraper script runs to gather data
              from the various websites and combine all of that into JSON and a
              ICS file. The last thing that script does is upload all of that
              information to my database using Sanity.io. New events are addded
              and old events removed every day. At 4am, the website itself
              rebuilds pulling all of that new data into to generate the various
              pages. Once the build process is complete, the new version of the
              site is deployed to my hosting solution, Netlify. That said, as
              long as you are viewing the site after about 4:05am each day, you
              are seeing the most updated information I have available. The
              whole process is automated and enables me to be hands off once the
              site is built unless I am adding new features or changing up the
              look or something.
            </Box>
            <Box sx={{ ...blurbStyles, mt: 4 }}>
              If you REALLY need to see the source code for this site, you can
              view that in the following places:
              <Box as="ul" ml={8}>
                <Box as="li" sx={{ maxWidth: '60rem', my: 3 }}>
                  <Link
                    href="https://gitlab.com/jgdigitaljedi/fallstown-festivities"
                    sx={{
                      display: 'flex',
                      alignItems: 'baseline',
                      fontweight: 'bold',
                      fontFamily: 'heading',
                      color: 'textHighlight',
                    }}
                    isExternal
                  >
                    Site source code
                    <FaExternalLinkSquareAlt
                      style={{ fontSize: '.75rem', marginLeft: '.5rem' }}
                    />
                  </Link>
                </Box>
                <Box as="li" sx={{ maxWidth: '60rem', my: 3 }}>
                  <Link
                    href="https://gitlab.com/jgdigitaljedi/wf-events-calendar"
                    sx={{
                      display: 'flex',
                      alignItems: 'baseline',
                      fontweight: 'bold',
                      fontFamily: 'heading',
                      color: 'textHighlight',
                    }}
                    isExternal
                  >
                    Events scraper source code
                    <FaExternalLinkSquareAlt
                      style={{ fontSize: '.75rem', marginLeft: '.5rem' }}
                    />
                  </Link>
                </Box>
              </Box>
            </Box>
          </Box>
        </Stack>
      </Stack>
    </Layout>
  );
};

export default About;

export const Head: HeadFC = () => (
  <SEO title={pageTitle} description={pageDesc} pathname="/about">
    <title id="page-title">{pageTitle}</title>
    <meta name="description" id="description" content={pageDesc} />
  </SEO>
);
