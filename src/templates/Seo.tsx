import React, { ReactNode } from 'react';
import { useSiteMetadata } from '../hooks/useSiteMetadata';
import { PageProps } from 'gatsby';

interface SeoProps {
  title: string;
  description: string;
  pathname: string;
  children: ReactNode;
}

export const SEO: React.FC<SeoProps> = ({
  title,
  description,
  pathname,
  children,
}) => {
  const {
    title: defaultTitle,
    description: defaultDescription,
    image,
    siteUrl,
  } = useSiteMetadata();

  const seo = {
    title: title || defaultTitle,
    description: description || defaultDescription,
    image: `${siteUrl}${image}`,
    url: `${siteUrl}${pathname || ``}`,
  };

  return (
    <>
      <meta name="image" content={seo.image} is="image" />
      <meta
        name="twitter:card"
        content="summary_large_image"
        id="twitterCard"
      />
      <meta name="twitter:title" content={seo.title} id="twitterTitle" />
      <meta name="twitter:url" content={seo.url} id="twitterUrl" />
      <meta
        name="twitter:description"
        content={seo.description}
        id="twitterDescription"
      />
      <meta name="twitter:image" content={seo.image} id="twitterImage" />
      {children}
    </>
  );
};
