import React, { ReactNode } from 'react';
import Navbar from '../components/Navbar';
import { Box, SystemStyleObject } from '@chakra-ui/react';
import ScrollToTop from '../components/ScrollToTop';
import Footer from '../components/Footer';
import { hexPattern } from '../util/styles';

interface LayoutProps {
  location: Location;
  children: ReactNode;
  noPadding?: boolean;
  sxProps?: SystemStyleObject;
  innerSxProps?: SystemStyleObject;
}

const Layout: React.FC<LayoutProps> = ({
  location,
  children,
  noPadding,
  sxProps,
  innerSxProps,
}) => {
  return (
    <Box
      id="top"
      sx={{ ...sxProps, bg: 'bg1', bgGradient: 'linear(to-b, bg1, bg2)' }}
    >
      <Navbar location={location} />
      <Box sx={hexPattern}></Box>
      <Box
        sx={{
          p: noPadding ? 0 : [2, 2, 8],
          minHeight: 'calc(100vh - 5rem)',
          zIndex: 5,
          ...innerSxProps,
        }}
      >
        {children}
      </Box>
      <Footer />
      <ScrollToTop />
    </Box>
  );
};

export default Layout;

export const Head = () => (
  <>
    <link
      rel="apple-touch-icon"
      sizes="180x180"
      href="/favicons/apple-touch-icon.png"
      id="apple-touch-icon"
    />
    <link
      rel="mask-icon"
      href="/safari-pinned-tab.svg"
      color="#1d5f7c"
      id="safari-pinned-tab"
    />
    <meta
      name="msapplication-TileColor"
      content="#1d5f7c"
      id="msapplication-TileColor"
    />
  </>
);
