import { extendTheme } from '@chakra-ui/react';
import { MultiSelectTheme } from 'chakra-multiselect';

export const myTheme = extendTheme({
  semanticTokens: {
    colors: {
      bg1: {
        default: 'gray.100',
        _dark: 'gray.900',
      },
      bg2: {
        default: 'gray.200',
        _dark: 'gray.700',
      },
      bg3: {
        default: '#e1e6ea',
        _dark: '#0a0d10',
      },
      bg4: {
        default: 'gray.300',
        _dark: 'gray.800',
      },
      textHighlight: {
        default: 'blue.800',
        _dark: 'blue.300',
      },
      textHighlight2: {
        default: 'orange.700',
        _dark: 'orange.300',
      },
      regularText: {
        default: 'black',
        _dark: 'white',
      },
      textInfo: {
        default: 'orange.800',
        _dark: 'orange.200',
      },
      textNext: {
        default: 'gray.800',
        _dark: 'gray.50',
      },
      cashola: {
        default: 'green.600',
        _dark: 'green.400',
      },
      ratings: {
        default: 'yellow.500',
        _dark: 'yellow.500',
      },
    },
  },
  components: {
    MultiSelect: MultiSelectTheme,
    Button: {
      baseStyle: {
        fontWeight: 'bold',
      },
      variants: {
        lighter: {
          backgroundColor:
            'linear-gradient(to right, orange.300 0, orange.500 100%)',
          transition: 'all ease 0.3s',
          ':hover': {
            transform: 'scale(1.08)',
            boxShadow: 'elevated',
            backgroundImage:
              'radial-gradient(circle at center, orange.700 0, orange.900 100%)',
          },
          ':active': {
            transform: 'scale(.98)',
            boxShadow: 'none',
            backgroundImage:
              'radial-gradient(circle at center, orange.500 0, orange.800 100%)',
          },
        },
        circle: {
          borderRadius: '50%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          p: 2,
          backgroundImage:
            'radial-gradient(circle at center, orange.600 0, orange.900 100%)',
          boxShadow: 'button',
          transition: 'all ease 0.3s',
          color: 'white',
          ':hover': {
            transform: 'scale(1.08)',
            boxShadow: 'elevated',
            backgroundImage:
              'radial-gradient(circle at center, orange.700 0, orange.900 100%)',
          },
          ':active': {
            transform: 'scale(.98)',
            boxShadow: 'none',
            backgroundImage:
              'radial-gradient(circle at center, orange.500 0, orange.800 100%)',
          },
        },
        contrast: {
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          p: 4,
          backgroundColor: 'bg1',
          color: 'regularText',
          ':hover': {
            backgroundColor: 'bg2',
          },
        },
      },
    },
  },
  config: {
    initialColorMode: 'system',
    useSystemColorMode: false,
  },
  fonts: {
    body: '"Lora", serif',
    heading: '"Oswald", sans-serif',
    mono: '"Menlo", monospace',
  },
  shadows: {
    text: '0 1px 2px rgba(0, 0, 0, 0.25), 0 2px 4px rgba(0, 0, 0, 0.125)',
    small: '0 1px 2px rgba(0, 0, 0, 0.0625), 0 2px 4px rgba(0, 0, 0, 0.0625)',
    button: '0 1px 2px rgba(0, 0, 0, 0.125), 0 4px 8px rgba(0, 0, 0, 0.250)',
    card: '0 4px 8px rgba(0, 0, 0, 0.125)',
    elevated: '0 1px 2px rgba(0, 0, 0, 0.125), 0 8px 12px rgba(0, 0, 0, 0.250)',
    top: '0px -2px 8px rgba(0,0,0,0.34), 0px -4px 16px rgba(0,0,0,0.17)',
  },
  colors: {
    black: '#040e13',
    white: '#ebf4fa',
    blue: {
      50: '#EAF5FA',
      100: '#C5E4F1',
      200: '#A0D3E8',
      300: '#7BC2E0',
      400: '#56B0D7',
      500: '#319FCE',
      600: '#277FA5',
      700: '#1D5F7C',
      800: '#144052',
      900: '#0A2029',
    },
    gray: {
      50: '#F0F2F5',
      100: '#D4DBE2',
      200: '#B9C4D0',
      300: '#9EADBD',
      400: '#8296AB',
      500: '#677F98',
      600: '#52667A',
      700: '#3E4C5B',
      800: '#29333D',
      900: '#15191E',
      950: '#010102',
    },
    red: {
      50: '#FBEAEA',
      100: '#F3C4C4',
      200: '#EB9F9E',
      300: '#E37977',
      400: '#DC5351',
      500: '#D42D2B',
      600: '#A92423',
      700: '#7F1B1A',
      800: '#551211',
      900: '#2A0909',
    },
    yellow: {
      50: '#FFF8E5',
      100: '#FFEBB8',
      200: '#FFDE8A',
      300: '#FFD15C',
      400: '#FFC42E',
      500: '#FFB700',
      600: '#CC9200',
      700: '#996E00',
      800: '#664900',
      900: '#332500',
    },
    cyan: {
      50: '#EEF7F7',
      100: '#CFE7E7',
      200: '#B1D8D8',
      300: '#92C9C9',
      400: '#73BABA',
      500: '#55AAAA',
      600: '#448888',
      700: '#336666',
      800: '#224444',
      900: '#112222',
    },
    teal: {
      50: '#E9FBF8',
      100: '#C2F5ED',
      200: '#9AEEE1',
      300: '#73E8D5',
      400: '#4CE1CA',
      500: '#24DBBE',
      600: '#1DAF98',
      700: '#168372',
      800: '#0F574C',
      900: '#072C26',
    },
    green: {
      50: '#EEF7F0',
      100: '#CFE7D4',
      200: '#B0D8B8',
      300: '#92C99C',
      400: '#73BA81',
      500: '#54AB65',
      600: '#438951',
      700: '#33663D',
      800: '#224428',
      900: '#112214',
    },
    orange: {
      50: '#FEEEE7',
      100: '#FCD1BA',
      200: '#FAB38E',
      300: '#F99562',
      400: '#F77736',
      500: '#F5590A',
      600: '#C44708',
      700: '#933506',
      800: '#622404',
      900: '#311202',
    },
    pink: {
      50: '#FEE7EC',
      100: '#FBBBC9',
      200: '#F990A6',
      300: '#F76483',
      400: '#F53860',
      500: '#F20D3E',
      600: '#C20A31',
      700: '#910825',
      800: '#610519',
      900: '#30030C',
    },
    purple: {
      50: '#F4F0F5',
      100: '#E1D4E3',
      200: '#CDB8D0',
      300: '#BA9DBE',
      400: '#A681AC',
      500: '#93659A',
      600: '#76517B',
      700: '#583D5C',
      800: '#3B293D',
      900: '#1D141F',
    },
  },
});

export default myTheme;
