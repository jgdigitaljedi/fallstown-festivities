export const hexPattern = {
  content: "''",
  position: 'absolute',
  width: '100%',
  height: '25rem',
  backgroundImage: `repeating-radial-gradient(#0000 0% 12%,#933506 13% 26% )`,
  backgroundSize: '100px 100px',
  mask: 'linear-gradient(to bottom, rgba(0, 0, 0, .1), rgba(0, 0, 0, 0))',
};

export const maxWidth = '100rem';
