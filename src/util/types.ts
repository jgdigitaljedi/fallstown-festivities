export interface CalendarEvent {
  address: string;
  calendar: string;
  categories: string[];
  dateTime: string;
  description: string;
  endTime: string;
  eventWebsite: string | null;
  eventsWebsite: string | null;
  geo: {
    lat: number;
    lon: number;
  };
  location: string;
  name: string;
  phone: string | null;
  startTime: string;
  title: string;
  website: string;
}

export interface SanityIcs {
  mimeType: string;
  originalFilename: string;
  url: string;
  _updatedAt: string;
}

export interface SanityData {
  allSanityEvent: {
    nodes: CalendarEvent[];
  };
  allSanityFileAsset: {
    nodes: SanityIcs[];
  };
}

export interface CalendarGridDay {
  dateString: string;
  dayOfMonth: number;
  isCurrentMonth: boolean;
  isPreviousMonth?: boolean;
  isNextMonth?: boolean;
}

export interface CalendarGridDayWithEvents extends CalendarGridDay {
  events: CalendarEvent[];
}

export interface HoursBreakdown {
  start: string;
  end: string;
  startSplit: string | null;
  endSplit: string | null;
}

export interface BusinessHours {
  day: string;
  breakdown: HoursBreakdown;
  display: string;
}

export interface SanityBusiness {
  address: string;
  name: string;
  id: string;
  imageUrl: string | null;
  yelpUrl: string;
  categories: string[];
  rating: number;
  reviewCount: number;
  city: string;
  coordinates: {
    lat: number;
    lon: number;
  };
  transactions: string[];
  price: string;
  location: string;
  phone: string;
  displayPhone: string;
  hours: BusinessHours[];
  menuUrl: string | null;
  type: string;
}
