import { BusinessHours, HoursBreakdown, SanityBusiness } from './types';
import sortBy from 'lodash/sortBy.js';
import intersection from 'lodash/intersection.js';
import dayjs from 'dayjs';
import isSameOrBeforeMod from 'dayjs/plugin/isSameOrBefore';
import isSameOrAfterMod from 'dayjs/plugin/isSameOrAfter';
import customParseFormatMod from 'dayjs/plugin/customParseFormat';
import { daysInOrder } from './constants';
import { Option } from 'chakra-multiselect';

dayjs.extend(isSameOrBeforeMod);
dayjs.extend(isSameOrAfterMod);
dayjs.extend(customParseFormatMod);

export const typeToDisplay = {
  restaurant: 'Restaurants',
  bar: 'Nightlife',
};

export const getBusinessTabs = (businesses: SanityBusiness[]): string[] => {
  return businesses
    .map((b: SanityBusiness) => b.type)
    .reduce((a: string[], b: string) => {
      if (!a.includes(b)) {
        a.push(b);
      }
      return a;
    }, []);
};

export const dataForType = (type: string, businesses: SanityBusiness[]) => {
  return sortBy(
    businesses.filter((b: SanityBusiness) => b.type === type),
    'name'
  );
};

const convertHour = (hour: string, dt: Date) => {
  const todayStr = `${dayjs(dt).format('YYYY-MM-DD')} ${hour}`;
  return dayjs(todayStr, 'YYYY-MM-DD hh:mm A');
};

export const isBusinessOpen = (hours: BusinessHours[]) => {
  const today = new Date();
  const day = daysInOrder[today.getDay()];
  const todayHours = hours.find((h) => h.day === day);
  const breakdown = todayHours?.breakdown;
  if (!breakdown || breakdown.start === 'Closed') {
    return false;
  }
  const inFirstRange =
    dayjs(today).isSameOrAfter(convertHour(breakdown.start, today)) &&
    dayjs(today).isSameOrBefore(convertHour(breakdown.end, today));
  if (breakdown.startSplit && breakdown.endSplit) {
    const inSecondRange =
      dayjs(today).isSameOrAfter(convertHour(breakdown.startSplit, today)) &&
      dayjs(today).isSameOrBefore(convertHour(breakdown.endSplit, today));
    return inFirstRange || inSecondRange;
  }
  return inFirstRange;
};

export const getCategories = (businesses: SanityBusiness[]): string[] => {
  return businesses
    .map((b: SanityBusiness) => b.categories)
    .reduce((a: string[], b: string[]) => {
      b.forEach((c: string) => {
        if (!a.includes(c)) {
          a.push(c);
        }
      });
      return a;
    }, []);
};

export const filterByCategory = (
  businesses: SanityBusiness[],
  categories: Option[]
) => {
  const cats = categories.map((c: Option) => c.value);
  return businesses.filter((bus: SanityBusiness) => {
    return intersection(cats, bus.categories).length > 0;
  });
};
