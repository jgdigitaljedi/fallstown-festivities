import * as React from 'react';
import { ChakraProvider } from '@chakra-ui/react';

// Import your extended theme from a location in `./src`
import myTheme from './src/@chakra-ui/gatsby-plugin/theme.js';

export const wrapRootElement = ({ element }) => (
  // Or ChakraBaseProvider if you only want to compile the default Chakra theme tokens
  <ChakraProvider theme={myTheme}>{element}</ChakraProvider>
);
